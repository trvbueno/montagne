#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "dllist.h"
#include "distance.h"

typedef struct node_s
{
	int val;
	struct node_s * prev;
	struct node_s * next;
} node;

struct dllist_s
{
	node * begin; // sentinel
	node * end;   // sentinel
};

dllist dllist_create(void)
{
	// create a list and its sentinel nodes
	dllist newls = NULL;
	newls = malloc(sizeof(struct dllist_s));
	if (newls != NULL)
	{
		newls->begin = NULL;
		newls->end   = NULL;
		newls->begin = malloc(sizeof(node));
		newls->end   = malloc(sizeof(node));
		if ((newls->end == NULL) || (newls->begin == NULL)) 
		{
			if (newls->end) free(newls->end);
			if (newls->begin) free(newls->begin);
			return NULL;
		}
		// and set all the pointers to what they should point to:
		newls->begin->prev = NULL;
		newls->begin->next = newls->end;
		newls->end->prev = newls->begin;
		newls->begin->prev = NULL;
		newls->begin->val = -1;
		newls->end->val = -1;

		return newls;
	}
	else return NULL;
}

bool dllist_empty(dllist const l)
{
	return (l->begin->next == l->end);
}

int dllist_push_front(dllist l, int v)
{
	node * newnod = NULL;
	newnod = malloc(sizeof(node));
	if (newnod != NULL)
	{
		newnod->val = v;
		newnod->next = l->begin->next;
		newnod->prev = l->begin;
		// embed the newly created node at beginning
		l->begin->next->prev = newnod;
		l->begin->next = newnod;

		return 1;
	}
	else return 0;
}

int dllist_push_back(dllist l, int v)
{
	// create a new node
	node * newnod = NULL;
	newnod = malloc(sizeof(node));
	if (newnod != NULL)
	{
		// we can assign the value to the new node
		newnod->val = v;
		newnod->next = l->end;
		newnod->prev = l->end->prev;
		// and embed it at the end
		l->end->prev->next = newnod;
		l->end->prev = newnod;

		return 1;
	}
	else return 0;
}

int dllist_pop_element(dllist l, int v)
{
	/* remove the first occurence beginning from top|front */
	node * nod = l->begin->next;
	while ((nod != l->end) && (nod->val != v)) nod = nod->next;
	if (nod != l->end)
	{
		/* then the v was found in list */
		nod->prev->next = nod->next;
		nod->next->prev = nod->prev;
		free(nod);
		return 1;
	}
	else return 0;
}

int dllist_pop_front(dllist l)
{
	int v;
	// just make the two nodes arround link together, short-circuit !
	node * nod = l->begin->next;
	v = nod->val;
	nod->next->prev = nod->prev;
	nod->prev->next = nod->next;
	free(nod);
	return v;
}


int dllist_pop_back(dllist l)
{
	int v;
	// idem, short-circuit
	node * nod = l->end->prev;
	v = nod->val;
	nod->next->prev = nod->prev;
	nod->prev->next = nod->next;
	free(nod);
	return v;
}

bool dllist_in(dllist l, int v)
{
	node * nod = l->begin->next;
	while ((nod != l->end) && (nod->val != v)) nod = nod->next;
	return (nod != l->end);
}

void dllist_free(dllist l)
{
	// clean all values before
	while (! dllist_empty(l)) dllist_pop_front(l);
	// and finally free the list
	free(l->end);
	free(l->begin);
	free(l);
}

void dllist_print(dllist l)
{
	node * nod = l->begin->next;
	while (nod->next != l->end)
	{
		printf("%d -> ", nod->val);
		nod = nod->next;
	}
	printf("%d\n", nod->val);
}

int dllist_push_prior (dllist l, float *f, int n, bool (*lower)(float, float))
{
	/* the array tells the priority of each element of the list
	 * with increasing priority when approaching 0.f */
	if (dllist_empty(l)) return dllist_push_back(l,n);
	else
	{
		node * nod = l->begin->next;
		while ( nod != l->end && lower(*(f+n), *(f+(nod->val))) )
			nod = nod->next;

		node * new_node = NULL;
		new_node = malloc(sizeof(node));

		if (new_node != NULL)
		{
			new_node->next = nod;
			new_node->prev = nod->prev;
			nod->prev->next = new_node;
			nod->prev = new_node;
			new_node->val = n;
			return 1;
		}
		else return 0;
	}
}
