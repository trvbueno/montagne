#include <stdio.h>
#include "array.h"

void array_print1d(int *tab, int l)
{
	for (int i = 0; i < l; ++i) printf("%d ", tab[i]);
	printf("\n");
}

void array_print2d(int *tab, int w, int h)
{
	for (int j = 0; j < h; ++ j)
	{
		for (int i = 0; i < h; ++ i)
		{
			printf("%d ", tab[i+(w*j)]);
		}
		printf("\n");
	}
}

void array_fill(int *tab, int l, int v)
{
	for (int i = 0; i < l; ++ i) tab[i] = v;
}
