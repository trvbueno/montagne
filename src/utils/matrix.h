/* This module set a number of functions to manipulate a matrix type */

#ifndef MATRIX_H
#define MATRIX_H

typedef float * matrix;

matrix matx_create(int w, int h);
// return a dumb pointer of an array int[w*h]

void matx_init(matrix m, int w, int h, float n_val);
// set every int of matrix to n_val

int matx_index(int i, int j, int w, int h);
// is used to return the index in the matrix

void matx_free(matrix m);
// DON'T FORGET TO FREE

void matx_print(matrix m, int w, int h);

#endif
