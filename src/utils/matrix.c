#include "matrix.h"

#include <stdio.h>
#include <stdlib.h>

matrix matx_create(int w, int h)
{
	matrix m = NULL;
	printf("creating %d-size matrix\n", w*h);
	m = malloc((w*h)*sizeof(float));
	if (m != NULL) return m;
	else return NULL;
}

void matx_init(matrix m, int w, int h, float n_val)
{
	for (int i = 0; i < h*w; ++i) m[i] = n_val;
}

int matx_index (int i, int j, int w, int h)
{
	return ((i>=0) && (i<w) && (j>=0) && (j<h)) ? ((w*j)+i) : -1;
}

void matx_free(matrix m)
{
	free(m);    // WAAOOOWW !!
}

void matx_print(matrix m, int w, int h)
{
	for (int j = 0; j < h; ++j)
	{
		for (int i = 0; i < w; ++i)
		{
			printf("%5f ", m[matx_index(i,j,w,h)]); // print the element m[i][j]
		}
		printf("\n");
	}
}
