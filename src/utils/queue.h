/* This module brings a dynamic list type usable on integer type by default */

#ifndef QUEUE_H
#define QUEUE_H

#include <stdbool.h>

struct queue_s
{
	int * data;
	int begin;
	int end;
	int sz;
};
typedef struct queue_s * queue;

queue queue_create (int size);
/* create a queue that MUST BE FREED after use */

bool queue_empty (queue const q);
bool queue_full (queue const q);
bool queue_in (queue q, int v);

int queue_push (queue q, int v);
int queue_pop (queue q);

void queue_free (queue q);

void queue_print (queue q);

#endif
