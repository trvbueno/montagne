/* This module brings a dynamic list type usable on integer type by default */

#ifndef DLLIST_H
#define DLLIST_H

typedef struct dllist_s *dllist;

dllist dllist_create (void);
/* create a dllist object that MUST BE FREED after use */

bool dllist_empty (dllist const l);
/* return if a list is empty or not */

int dllist_push_front (dllist l, int v);
int dllist_push_back (dllist l, int v);
/* add a new integer to the list */

int dllist_pop_element (dllist l, int v);
int dllist_pop_front (dllist l);
int dllist_pop_back (dllist l);
/* pop an element in the list */

bool dllist_in (dllist l, int v);
/* return true if v is found in the list */

void dllist_free (dllist l);
/* just free the whole list - handle the free in the structure */

void dllist_print (dllist l);

int dllist_push_prior (dllist l, float *f, int n, bool (*lower)(float, float));

#endif
