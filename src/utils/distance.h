#ifndef DISTANCE_H
#define DISTANCE_H

/* for grid and general representation */
#define D1 1.0
#define D2 1.4121

#define INFINITE -1.0

bool dist_lower (float a, float b);

#endif
