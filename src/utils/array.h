#ifndef ARRAY_H
#define ARRAY_H

// print a 1d array
void array_print1d(int *tab, int l);
// print a 2d array
void array_print2d(int *tab, int w, int h);
// fill an array with v
void array_fill   (int *tab, int l, int v);

#endif