#include <stdio.h>
#include <stdlib.h>

#include "queue.h"

queue queue_create (int size)
{
	queue q = NULL;
	if ((q = malloc(sizeof(struct queue_s))) == NULL)
		exit(EXIT_FAILURE);
	else
	{
		if ((q->data = malloc((++size)*sizeof(int))) == NULL)
			exit(EXIT_FAILURE);
		q->begin = 0;
		q->end = 0;
		q->sz = size;
		return q;
	}
}

bool queue_empty (queue const q)
{
	return (q->end == q->begin);
}

bool queue_full (queue const q)
{
	return (q->begin == (q->end+1)%q->sz);
}

bool queue_in (queue q, int v)
{
	bool found = false;
	for (int i = q->begin; (i != q->end) && (!found); i = (i+1)%(q->sz))
	{
		if (q->data[i] == v) found = true;
	}
	return found;
}

int queue_push (queue q, int v)
{
	if (! queue_full(q))
	{
		q->data[q->end] = v;
		q->end = ((q->end+1) % q->sz);
		return 1;
	}
	else return 0;
}

int queue_pop (queue q)
{
	if (! queue_empty(q))
	{
		int val = q->data[q->begin];
		q->begin = ((q->begin+1) % q->sz);
		return val;
	}
	else return 0;
}

void queue_free (queue q)
{
	free(q->data);
	free(q);
}

void queue_print (queue q)
{
	for (int i = q->begin; i != q->end; i = ((i+1) % (q->sz)))
		printf("%d ", q->data[i]);
	printf("\n");
}

