#ifndef LOCATION_H
#define LOCATION_H

#include "map.h"

/* location is incomplete and is currently just a simulation */

typedef struct location_s{
	point_t pos;
	float angle;
} location_t;

void location_init();
location_t location_get();
location_t location_stdin(); /* command-line testing */
void location_set(location_t loc);

#endif