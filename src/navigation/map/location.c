#include "location.h"

static location_t location_main;

void location_init()
{
	/* TODO later : decawave support */

	location_main.pos.x = 0.0f;
	location_main.pos.y = 0.0f;
	location_main.angle = 0.0f;
}

location_t location_get()
{
	return location_main;
}

location_t location_stdin()
{

	printf("point and rotation: ");
	scanf("%f %f %f", &(location_main.pos.x), &(location_main.pos.y), &(location_main.angle));
	
	return location_main;
}

void location_set(location_t loc)
{
	location_main = loc;
}