#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include "map.h"
#include "configuration.h"

static map_t *main_map;

/* Basically it works as follow:
 * - the parsing process start
 * - at every scalar event the parse_map_simple() is called
 *     - if the scalar match a field of map_t, just affect it
 *     - else if it is the beginning of the objects section, call parse_object()
 *          - process is similar, but inside the object structure
 *
 * Every apparently useless yaml_parse are essentials, as they manage all the
 * non-data events like START MAPPING and co. If one of these sequence miss,
 * the parser just crash */

bool map_init()
{
	main_map = map_import_room_yaml(MAP_ACCESS_PATH);

	return (main_map != NULL);
}

map_t *map_import_room_yaml (char *file)
{
	map_t *map = NULL;
	if (! (map = malloc(sizeof(map_t)))) return NULL;

	/* open the yaml source file */
	FILE * f_source = NULL;
	f_source = fopen(file, "rt");
	// int i = 0;

	/* declare the yaml components */
	yaml_parser_t parser;
	yaml_event_t event;

	/* check wether or not all were well initialized */
	if (f_source == NULL)
	{
		perror("yaml source");
		free(map);
		return NULL;
	}

	if (! yaml_parser_initialize(&parser))
	{
		fprintf(stderr, "error in parser initialization\n");
		free(map);
		return NULL;
	}

	/* set the parser's source file */
	yaml_parser_set_input_file(&parser, f_source);

	/* parsing part of the procedure */
	do
	{
		/* parse a line and at the same check success */
		if (yaml_parser_parse(&parser, &event) == 0)
		{
			printf("parser error :(\n");
		}

		/* and act in consequence of the event */
		if (event.type == YAML_SCALAR_EVENT)
			map_parse_map_simple(&parser, &event, map);


		/* and delete the event if it's not the end event */
		if (event.type != YAML_STREAM_END_EVENT) yaml_event_delete(&event);
	}
	while (event.type != YAML_STREAM_END_EVENT);

	/* free the last event */
	yaml_event_delete(&event);

	/* end of the parsing */

	/* close everything correctly */
	yaml_parser_delete(&parser);
	fclose(f_source);
	return map;
}

/* sub parsing procedures */
void map_parse_map_simple(yaml_parser_t *parser, yaml_event_t *event, map_t *map)
{
	int i;

	if (strcmp((char *) event->data.scalar.value, "place") == 0)
	{
		/* parse the next element to find the value */
		yaml_event_delete(event);
		yaml_parser_parse(parser, event);
		strncpy(map->name, (char *) event->data.scalar.value, MAX_NAME);
	}
	else if (strcmp((char *) event->data.scalar.value, "width") == 0)
	{
		yaml_event_delete(event);
		yaml_parser_parse(parser, event);
		map->width = strtof((char *) event->data.scalar.value, NULL);
	}
	else if (strcmp((char *) event->data.scalar.value, "height") == 0)
	{
		yaml_event_delete(event);
		yaml_parser_parse(parser, event);
		map->height = strtof((char *) event->data.scalar.value, NULL);
	}
	else if (strcmp((char *) event->data.scalar.value, "unit_subdiv") == 0)
	{
		yaml_event_delete(event);
		yaml_parser_parse(parser, event);
		map->m_unit = (int) strtol((char *) event->data.scalar.value, NULL, 10);
	}
	else if (strcmp((char *) event->data.scalar.value, "sensor1") == 0)
	{
		yaml_event_delete(event);
		yaml_parser_parse(parser, event);
		map_parse_point(parser, event, &(map->sensor1));
	}
	else if (strcmp((char *) event->data.scalar.value, "sensor2") == 0)
	{
		yaml_event_delete(event);
		yaml_parser_parse(parser, event);
		map_parse_point(parser, event, &(map->sensor2));
	}
	else if (strcmp((char *) event->data.scalar.value, "sensor3") == 0)
	{
		yaml_event_delete(event);
		yaml_parser_parse(parser, event);
		map_parse_point(parser, event, &(map->sensor3));
	}
	else if (strcmp((char *) event->data.scalar.value, "margin") == 0)
	{
		yaml_event_delete(event);
		yaml_parser_parse(parser, event);
		map->margin = strtof((char *) event->data.scalar.value, NULL);
	}
	else if (strcmp((char *) event->data.scalar.value, "object_number") == 0)
	{
		yaml_event_delete(event);
		yaml_parser_parse(parser, event);
		map->obj_count = (int) strtol((char*)event->data.scalar.value, NULL,10);
		if (map->obj_count > 0)
		{
			if (! (map->obj = malloc(map->obj_count*sizeof(object_t))))
			{
				free(map);
				exit(EXIT_FAILURE);
			}
		}
	}
	else if (strcmp((char *) event->data.scalar.value, "objects") == 0)
	{
		/* pop the START EVENT */
		yaml_event_delete(event);
		yaml_parser_parse(parser, event);
		/* for every objects, parse the object */
		for (i=0; i < map->obj_count; ++i)
			map_parse_object(parser, event, (map->obj)+i);
	}
	else
	{
		fprintf(stderr, "No attribute %s in map\n",
		        (char *) event->data.scalar.value);
		if (map->obj) free(map->obj);
		free(map);
		EXIT_FAILURE;
	}
}

/* this function handles the parsing of a single object and its points */
void map_parse_object(yaml_parser_t *parser, yaml_event_t *event, object_t *obj)
{
	int i, stop = 0;

	/* pop the MAP START EVENT at beginning of an object */
	yaml_event_delete(event);
	yaml_parser_parse(parser, event);

	while (1)
	{
		/* get the attribute */
		yaml_event_delete(event);
		yaml_parser_parse(parser, event);

		if (event->type == YAML_MAPPING_END_EVENT)
			break;

		/* and analyse it */
		if (strcmp((char *) event->data.scalar.value, "name") == 0)
		{
			yaml_event_delete(event);
			yaml_parser_parse(parser, event);
			strncpy(obj->name, (char *) event->data.scalar.value, MAX_NAME);
		}
		else if (strcmp((char *) event->data.scalar.value, "desc") == 0)
		{
			yaml_event_delete(event);
			yaml_parser_parse(parser, event);
			strncpy(obj->description, (char *) event->data.scalar.value, MAX_DESCRIPTION);
		}
		else if (strcmp((char *) event->data.scalar.value, "door") == 0)
		{
			yaml_event_delete(event);
			yaml_parser_parse(parser, event);
			map_parse_point(parser, event, &(obj->door));
		}
		else if (strcmp((char *) event->data.scalar.value, "nb_points") == 0)
		{
			yaml_event_delete(event);
			yaml_parser_parse(parser, event);
			obj->nb_points = strtol((char *) event->data.scalar.value, NULL,10);
		}
		else if (strcmp((char *) event->data.scalar.value, "points") == 0)
		{
			/* first throw away the SEQ START EVENT */
			yaml_event_delete(event);
			yaml_parser_parse(parser, event);

			/* get the sequence of points. the loop stops at i wich is
			 * initialized to 0, therefore any yaml generator shall ensure that
			 * the object's number of point is parsed before this step */
			for (i = 0; i < obj->nb_points; ++i)
			{
				yaml_event_delete(event);
				yaml_parser_parse(parser, event);
				map_parse_point(parser, event, obj->mesh + i);
			}
			/* and throw away the SEQ END EVENT */
			yaml_event_delete(event);
			yaml_parser_parse(parser, event);
		}
		else
		{
			fprintf(stderr,"No attribute %s in object\n",
			        (char *) event->data.scalar.value);
			stop = 5;
		}
	}
}

/* this function handles the parsing of a single point */
void map_parse_point(yaml_parser_t *parser, yaml_event_t *event, point_t *p)
{
	/* get rid of the starting event */
	yaml_event_delete(event);
	yaml_parser_parse(parser, event);

	/* parse a sequence of two element x and y (float), here the order where
	 * x and y is given is important: x -> y */
	p->x = strtof((char *) event->data.scalar.value, NULL);

	yaml_event_delete(event);
	yaml_parser_parse(parser, event);
	p->y = strtof((char *) event->data.scalar.value, NULL);

	/* same for the ending event */
	yaml_event_delete(event);
	yaml_parser_parse(parser, event);
}

bool map_accessible(map_t *const m, point_t *const p)
{
	bool ok = true;
	/* for every object in the map check it is outside */
	int obj = 0;
	while ((obj < m->obj_count) && ok)
	{
		ok = (! in_object(m->obj+obj, p)) && ok;
		++ obj;
	}

	return ok;
}

bool map_inside_margin(map_t *const m, point_t *const p)
{
	/* this function checks accessiblity on 8 points of the circle
	 * of center p and radius margin */
	bool res = false;
	const float diag = 0.70710678118; // == sin(45) == cos(45)
	point_t shifted_p;

	shifted_p.x = p->x;
	shifted_p.y = p->y + m->margin;
	res = res || (! map_accessible(m, &shifted_p));
	shifted_p.y = p->y - m->margin;
	res = res || (! map_accessible(m, &shifted_p));

	shifted_p.y = p->y;
	shifted_p.x = p->x + m->margin;
	res = res || (! map_accessible(m, &shifted_p));
	shifted_p.x = p->x - m->margin;
	res = res || (! map_accessible(m, &shifted_p));

	shifted_p.x = p->x + diag*m->margin;
	shifted_p.y = p->y + diag*m->margin;
	res = res || (! map_accessible(m, &shifted_p));
	shifted_p.y = p->y - diag*m->margin;
	res = res || (! map_accessible(m, &shifted_p));

	shifted_p.x = p->x - diag*m->margin;
	shifted_p.y = p->y + diag*m->margin;
	res = res || (! map_accessible(m, &shifted_p));
	shifted_p.y = p->y - diag*m->margin;
	res = res || (! map_accessible(m, &shifted_p));

	return res;
}

bool in_object(object_t *const obj, point_t *const p)
{
	int cross_count = 0;       /* the counter for contacts with object */
	/* compute the driving coefficient of the line between (0,0) and p */

	for (int i = 0; i < obj->nb_points; ++i)
		if (cross_segment(*p, obj, i, (i+1)%(obj->nb_points), (cross_count%2==1)))
			++ cross_count;

	if ((cross_count % 2) == 0) return false;
	else return true;
}

bool cross_segment(point_t dest, object_t *const obj, int op, int oq, bool ins)
{
	const float epsilon = 0.0001;
	point_t i, p, q;
	p = obj->mesh[op];
	q = obj->mesh[oq];

	/* find the line parameters from a to b */
	float segment_a, segment_b;

	i.x = dest.x+2*epsilon;
	i.y = dest.y+2*epsilon;
	if (p.x != q.x)
	{
		segment_a = (q.y - p.y)/(q.x - p.x);
		segment_b = p.y - (segment_a * p.x);
		i.y = (segment_a * i.x) + segment_b;
	}
	else return false; /* mean the segment is vertical */

	/* find the intersection */

	/* check the distance */
	if (dest.y < i.y) return false;

	/* and check if the point is inside the bounding box of p and q */
	if ( (i.x <= fmin(p.x, q.x)-epsilon) || (i.x >= fmax(p.x, q.x)+epsilon) ||
		 (i.y <= fmin(p.y, q.y)-epsilon) || (i.y >= fmax(p.y, q.y)+epsilon) )
		return false;

	/* still handle the case where it is a point */
	if ((i.x > q.x - epsilon) && (i.x < q.x + epsilon) &&
		(i.y > q.y - epsilon) && (i.y < q.y + epsilon))
		if ((obj->mesh[(oq+1)%obj->nb_points].x - q.x)*(p.x - q.x) < 0)
			return false;

	return true;
}

/* utilities */
float map_distance_A_to_B(point_t A, point_t B)
{
	return sqrtf(((B.x - A.x)*(B.x - A.x)) + ((B.y - A.y)*(B.y - A.y)));
}

float map_angle_A_to_B(point_t A, point_t B)
{
	/* atan(dy/dx) */
	float alpha = atan2f(B.y - A.y, B.x - A.x);

	/* and convert in degrees, on ]-180, 180] */
	return (alpha)*(180.f/M_PI);
}

/* gravity center */
point_t map_object_dumb_center(object_t *obj)
{
	point_t gravity_center = {0.f, 0.f};

	for (int p = 0; p < obj->nb_points; ++p)
	{
		gravity_center.x += obj->mesh[p].x;
		gravity_center.y += obj->mesh[p].y;
	}
	gravity_center.x /= (float) obj->nb_points;
	gravity_center.y /= (float) obj->nb_points;

	return gravity_center;
}

/* mass center */
point_t map_object_mass_center(object_t *obj)
{
	float area = 0.f, loop_area;
	point_t mass_center = {0.f, 0.f};

	for (int i=0; i < obj->nb_points-1; i++)
	{
		loop_area = ((obj->mesh[i].x)*(obj->mesh[i+1].y))-((obj->mesh[i].y)*(obj->mesh[i+1].x));

		mass_center.x += ((obj->mesh[i].x)+(obj->mesh[i+1].x))*loop_area;
		mass_center.y += ((obj->mesh[i].y)+(obj->mesh[i+1].y))*loop_area;
		
		area += loop_area;
	}

	area /= 2.f;
	mass_center.x /= (area*6.f);
	mass_center.y /= (area*6.f);

	return mass_center;
}

map_t *map_get_global()
{
	return main_map;
}

void map_free(map_t *m)
{
	free(m->obj);
	free(m);
}