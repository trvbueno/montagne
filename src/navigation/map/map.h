#ifndef T_MAP_H
#define T_MAP_H

#define MAX_POINT 16
#define MAX_NAME 30
#define MAX_DESCRIPTION 200

#define M_PI 3.14159265359

#include <yaml.h>
#include <stdbool.h>

/* structure definition used in map representation */

/* positionning types:
 *  + a point is for the real space localisation
 *  + a vertex is for the same but on the grid system */

typedef struct point_s
{
	float x;
	float y;
} point_t;

typedef struct object_s
{
	char name[MAX_NAME];               /* name, .ie "door 1", "conference"... */
	char description[MAX_DESCRIPTION]; /* small description */
	int nb_points;                     /* number of points in the object */
	point_t mesh[MAX_POINT];           /* set of points that draws the object */
	point_t door;
} object_t;

typedef struct map_s
{
	char name[MAX_NAME];               /* name of the exhibition room */

	float width;                       /* set in meters */
	float height;                      /* set in meters */
	int m_unit;                        /* number of units per meter */
	float margin;                      /* margin with objects and walls */

	point_t sensor1;                   /* position of the decawave® tag n°1 */
	point_t sensor2;                   /* position of the decawave® tag n°2 */
	point_t sensor3;                   /* position of the decawave® tag n°3 */

	int obj_count;                     /* number of objects in the room */

	object_t *obj;                     /* array of objects */
} map_t;
/* it could be clever to add the closest emergency exit to prevent the user from
 * manually select it him(her)self in panic, wich would be stupid.
 * but still remember: this is a prototype */



/* parse the yaml config file */

bool map_init (void);

/* return the map with new content */
map_t *map_import_room_yaml (char *file);

/* parse a field in the map, calls subfunctions when reach an object */
void map_parse_map_simple(yaml_parser_t *parser, yaml_event_t *event, map_t *map);

/* parse an object field */
void map_parse_object(yaml_parser_t *parser, yaml_event_t *event, object_t *obj);

/* just set x and y field */
void map_parse_point(yaml_parser_t *parser, yaml_event_t *event, point_t *p);


/* point-in-polygon */

/* return wether or not a point raisable in the map */
bool map_accessible(map_t *const m, point_t *const p);

/* use the above function to tell if a point is near an object */
bool map_inside_margin(map_t *const m, point_t *const p);

/* internal functions */
bool in_object(object_t *const obj, point_t *const p);

bool cross_segment(point_t dest, object_t *const obj, int op, int oq, bool ins);


/* useful functions */

/* distance from A to B */
float map_distance_A_to_B(point_t A, point_t B);

/* angle of the slope of line A to B, compared with positive x-axis */
float map_angle_A_to_B(point_t A, point_t B);

/* dumb center of an object (set of points) */
point_t map_object_dumb_center(object_t *obj);

/* mass center of an object (set of points) */
point_t map_object_mass_center(object_t *obj);

map_t *map_get_global();

/* free the map structure */
void map_free(map_t *m);

#endif