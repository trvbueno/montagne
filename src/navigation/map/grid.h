#ifndef GRID_H
#define GRID_H

#include "map.h"
#include "matrix.h"

/* this module provides functions to abstract the use of a grid layout along
 * with graphs used to compute path.
 *
 * as a convention, it is given that a taxicab tile unit is 1/UNIT_SUBDIV large
 * i.e there is UNIT_SUBDIV tiles/meter
 *
 * also this code will create the nodes from 0 to n-1 starting from the lower
 * left corner to right and then up */

int grid_nnodes(float length_meter, int subdiv_meter);

int grid_nodex(int nod, int w, int h);

int grid_nodey(int nod, int w, int h);

void grid_neighbor(int nod[], int w, int h, int n);
/* every non-relevant neighbor shall be -1 */

void grid_node_coord(map_t *map, int nod, int graph_w, int graph_h, float *x, float *y);

int grid_closest_node(map_t *map, point_t p);

int grid_closest_node_failsafe (map_t *map, matrix adj, int adjW, int adjH, point_t p);

void grid_gridify(map_t *map, matrix *adj, int *adj_width, int *adj_height);

#endif