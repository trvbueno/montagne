#include <stdbool.h>

#include "grid.h"
#include "distance.h"

int grid_nnodes (float length_meter, int subdiv_meter)
{
	// represents the number of tiles in the room with UNIT_SUBDIV subdivisions
	return (length_meter*subdiv_meter)-1;
}

int grid_nodex (int nod, int w, int h)
{
	// return the X index of node in the GRID graph
	if ((nod < w*h) && (nod >= 0)) return (nod % w);
	else return -1;
}

int grid_nodey (int nod, int w, int h)
{
	// return the Y index of node in the GRID graph
	if ((nod < w*h) && (nod >= 0)) return (nod / w);
	else return -1;
}

void grid_neighbor(int nod[], int w, int h, int nod_ref)
{
	int nx=grid_nodex(nod_ref,w,h), ny=grid_nodey(nod_ref,w,h), n, i=0;
	// for each node arround nod_ref
	for (int x = -1; x < 2; ++x)
	{
		for (int y = -1; y < 2; ++y)
		{
			n = nod_ref + x + (w*y);
			// if it isn't nod_ref itself
			if ((x != 0) || (y != 0))
			{
				// check the existence
				if ((nx+x >= 0) && (nx+x < w) && (ny+y >= 0) && (ny+y < h))
				{
					nod[i] = n;
				}
				// else mean that the node doesn't exits
				else nod[i] = -1;
				// and go to next index
				++ i;
			}
		}
	}
}

void grid_node_coord(map_t *map, int nod, int graph_w, int graph_h, float *x, float *y)
{
	float dist = 1.f/((float) map->m_unit);
	*x = (grid_nodex(nod, graph_w, graph_h)+1) * dist;
	*y = (grid_nodey(nod, graph_w, graph_h)+1) * dist;
}

int grid_closest_node (map_t *map, point_t p)
{
	float dist = 1.f/((float) (2*map->m_unit));
	int closest_x, closest_y;
	int off_x, off_y;

	/* if the point is in the grid proximity */
	if ((p.x > dist) && (p.x < map->width - dist) &&
		(p.y > dist) && (p.y < map->height - dist))
	{
		closest_x = (int) ((p.x - dist)/(2.f*dist));
		closest_y = (int) ((p.y - dist)/(2.f*dist));
		/* return the closest node */
		return closest_x + (grid_nnodes(map->width,map->m_unit) * closest_y);
	}
	
	else
	if ((p.x > 0) && (p.x < map->width) && (p.y > 0) && (p.y < map->height))
	{
		/* shift the position in the boundary of nodes */
		off_x = (p.x < dist) ? 1 : -1;
		off_y = (p.y < dist) ? 1 : -1;
		closest_x = (int) (((p.x+(off_x*dist)) - dist)/(2.f*dist));
		closest_y = (int) (((p.y+(off_y*dist)) - dist)/(2.f*dist));
		return closest_x + (grid_nnodes(map->width,map->m_unit) * closest_y);
	}
	else return -1;
}

int grid_closest_node_failsafe (map_t *map, matrix adj, int adjW, int adjH, point_t p)
{
	/* find the really closest node, no care if it's accessible or not */
	int realnode = grid_closest_node(map, p);

	/* the zone in wich we seek the nearest linked node in */
	int realy, realx;
	int area = 1, off_x, off_y, newnode;
	bool connected = false;

	// find the indexes of the node
	realx = grid_nodex(realnode, adjW, adjH);
	realy = grid_nodey(realnode, adjW, adjH);

	for (int i = 0; i < adjW*adjH; ++i)
		connected = (connected || (adj[matx_index(realnode, i, adjW*adjH, adjW*adjH)] > 0));
	if (connected) return realnode;

	/* and find a connected one */
	while ((! connected) && (area < 5))
	{
		off_x = -1;
		while ((! connected) && (off_x < 2))
		{
			off_y = -1;
			while ((! connected) && (off_y < 2))
			{
				// try a node
				newnode = realnode + area*(off_x + (off_y * adjW));

				// check if it is a existing and relevant node
				if ((grid_nodey(newnode, adjW, adjH) == realy) || (grid_nodex(newnode, adjW, adjH) == realx))
				{
					// and check if it is connected to another, i.e it is accessible
					for (int i = 0; i < adjW*adjH; ++i)
						connected = (connected || (adj[matx_index(newnode, i, adjW*adjH, adjW*adjH)] > 0));

					if (connected) return newnode;
				}

				++ off_y;
			}
			++ off_x;
		}
		++ area;         // expand the research area
	}

	return -1;
}

/* this procedure create an adjacence matrix adj */
void grid_gridify (map_t *map, matrix *adj, int *adj_width, int *adj_height)
{
	int node_number, neighbor;
	int *node_accessible;
	point_t coord;
	int x, y;

	// if the matrix already exists, do nothing
	if (*adj) return;

	// compute the number of needed nodes
	*adj_width = grid_nnodes(map->width, map->m_unit);
	*adj_height = grid_nnodes(map->height, map->m_unit);

	node_number = (*adj_width)*(*adj_height);
	node_accessible = calloc(node_number,sizeof(int));

	// create the adjacence matrix
	*adj = matx_create(node_number, node_number);

	// for each node, check if it is out of every object
	for (int nod = 0; nod < node_number; ++nod)
	{
		grid_node_coord(map,nod,*adj_width,*adj_height,&(coord.x),&(coord.y));
		node_accessible[nod] = map_accessible(map, &coord) && (! map_inside_margin(map, &coord));
	}

	for (int a = 0; a < node_number; ++a)
		for (int b = 0; b < node_number; ++b)
			(*adj)[matx_index(a, b, node_number, node_number)] = 0.f;

	// and for each node
	for (int nod = 0; nod < node_number; ++nod)
	{
		// if the node is accessible
		if (node_accessible[nod])
		{
			x = grid_nodex(nod, *adj_width, *adj_height);
			y = grid_nodey(nod, *adj_width, *adj_height);
			// for all neighbors in 8-neighborhood
			for (int i = -1; i < 2; ++i)
			{
				for (int j = -1; j < 2; ++j)
				{
					if ((x+i >= 0) && (x+i < *adj_width) &&
						(y+j >= 0) && (y+j < *adj_height))
					{
						// find the designation for the current neighbor
						neighbor = nod + i + ((*adj_width) * j);
						// if it isn't the node itself
						if ( ((i != 0) || (j != 0)) && (neighbor >= 0) &&
							(neighbor < (*adj_height) * (*adj_width)) &&
							node_accessible[neighbor])
						{
							// write in the adjacence matrix the link
							if ((i==0) || (j==0))
								(*adj)[matx_index(nod, neighbor, node_number, node_number)] = D1;
							else
								(*adj)[matx_index(nod, neighbor, node_number, node_number)] = D2;
						}
					}
				}
			}
		}
	}

	free(node_accessible);
}