#ifndef HEURIST_PATHF_H
#define HEURIST_PATHF_H

// define a function pointer type heuristic to be able to refer to any heuristic
typedef float (*heurstc)(int, int, int, int);

// both function return (float) distance between node a and node b
float heurstc_manhattan (int a, int b, int w, int h);
float heurstc_diagonal (int a, int b, int w, int h); // chebyshev distance

#endif