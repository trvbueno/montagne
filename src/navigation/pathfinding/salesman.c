#include <stdbool.h>
#include "salesman.h"

matrix salesman_proxy_graph(map_t *m)
{
	point_t from, to;

	/* create the adjacence matrix */
	matrix adj = matx_create(m->obj_count, m->obj_count);

	/* for each object, compute the distance with every others */
	for (int orig = 0; orig < m->obj_count; ++orig)
	{
		/* the door of the object */
		from = m->obj[orig].door;

		for (int dest = 0; dest < m->obj_count; ++dest)
		{
			if (orig != dest)
			{
				/* the gravity center of destination object */
				to = m->obj[dest].door;

				/* write in the adjacence matrix */
				adj[matx_index(orig, dest,m->obj_count,m->obj_count)] = map_distance_A_to_B(from, to);
			}
		}
	}

	return adj;
}

path_t salesman_greedy_path(matrix prox_graph, int nb_nodes, int start, int *l)
{
	int node_i = 1, from, to;
	bool *open = NULL;
	float min;

	/* return the length, too */
	*l = 0;

	path_t p;
	p.size = 1;
	p.path = NULL;
	if ( !(p.path = malloc(nb_nodes*sizeof(int))) ) return p;

	if ( !(open = malloc(nb_nodes*sizeof(bool))) )
	{
		free(p.path);
		return p;
	}

	/* set all points to open status except the starting one */
	for (int i = 0; i < nb_nodes; ++i) open[i] = true;

	/* the first node is the starting node */
	open[start] = false;
	p.path[0] = start;
	from = start;

	while (node_i < nb_nodes)
	{
		/* find the closest open vertex */
		min = -1.f;
		for (int i = 0; i<nb_nodes; ++i)
		{
			if (open[i] && ((min < 0) || (min > prox_graph[matx_index(from, i, nb_nodes, nb_nodes)])))
			{
				to = i;
				min = prox_graph[matx_index(from, to, nb_nodes, nb_nodes)];
			}
		}

		/* set the closed status */
		open[to] = false;

		/* and append to the path's length */
		*l += min;

		/* and write in the path */
		p.path[node_i] = to;
		p.size += 1;

		from = to;
		++ node_i;
	}

	return p;
}

/* this is an in-test function, use the greedy-path() with a */
/* defined starting point instead */
path_t salesman_shortest_greedy(matrix prox_graph, int nb_nodes)
{
	int start = 0, min = -1, distance = 0;
	path_t p, real_p;

	for (; start < nb_nodes; ++ start)
	{
		p = salesman_greedy_path(prox_graph, nb_nodes, start, &distance);
		if ((min < 0) || (distance < min))
		{
			if (real_p.path) free(real_p.path);
			real_p = p;
			min = distance;
		}
		else
		{
			if (p.path) free(p.path);
		}
	}
}

void salesman_prox_to_grid(map_t *m, path_t psource)
{
	int match = NULL;
	point_t point;


	for (int i = 0; i < psource.size; ++i)
	{
		point = m->obj[psource.path[i]].door;
		psource.path[i] = grid_closest_node_failsafe(m, gps_get_adjgrid(), gps_get_adjW(), gps_get_adjH(), point);
	}
}
