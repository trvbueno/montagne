#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "astar.h"
#include "dllist.h"
#include "distance.h"
#include "map/grid.h"
#include "heuristic.h"

path_t astar_path(matrix adj_mat, int graph_w, int graph_h, int A, int B)
{
	int current, nghb[8] = {0,0,0,0,0,0,0,0};
	// current will represent the current node at each step of loop
	// nghb is used to know every node in direct neighborhood of current
	int graph_sz = graph_h * graph_w;
	// also declare a volatile
	float cost;

	/* we create the two sets useful for the procedure:
	 *    open contains all the nodes directly to explore
	 *    closed contains all the nodes already explored */
	dllist openset = dllist_create();
	// here a double linked list is used to manage priority of nodes

	bool * closedset = NULL;
	if ((closedset = malloc(graph_sz*sizeof(bool))) == NULL) exit(EXIT_FAILURE);

	// and now an array which, for each node, tells what is its parent
	int * parent = NULL;
	if ((parent = malloc(graph_sz*sizeof(int))) == NULL) exit(EXIT_FAILURE);

	// and also save all g_vals for the nodes
	float * g_val = NULL, * f_val;

	if ((g_val = malloc(graph_sz*sizeof(float))) == NULL) exit(EXIT_FAILURE);
	else for (int i = 0; i < graph_sz; ++i) g_val[i] = INFINITE;

	if ((f_val = malloc(graph_sz*sizeof(float))) == NULL) exit(EXIT_FAILURE);
	/*else */for (int i = 0; i < graph_sz; ++i) f_val[i] = INFINITE;

	// tell the program our h(x) is the diagonal -> see heuristic.h
	heurstc h = heurstc_diagonal;

	// INIT
	dllist_push_front(openset, A);
	g_val[A] = 0;
	f_val[A] = 0;
	for (int i = 0; i < graph_sz; ++i) closedset[i] = false;


	// MAIN LOOP
	while ( (! dllist_empty(openset)) &&
	        ((current = dllist_pop_back(openset)) != B) )
	{
		// remove it from nodes unexplored and set it in explored ones
		closedset[current] = true;

		// for each node in the direct neighborhood
		grid_neighbor(nghb, graph_w, graph_h, current);
		for (int i = 0; i < 8; ++ i)
		{
			/* every neighbors not in closed */
			if ( (nghb[i] != -1) &&
			        (adj_mat[matx_index(current,nghb[i],graph_sz,graph_sz)] !=0.f) &&
			        (! closedset[nghb[i]]) )
			{
				// compute the g_val to this neighbor from current
				cost=(float)adj_mat[matx_index(current,nghb[i],graph_sz,graph_sz)]
				     + g_val[current];
				// and if the new cost is better or if the node isn't in openset
				if ( (! dllist_in(openset, nghb[i]))
				        || dist_lower(cost,g_val[nghb[i]]) )
				{
					parent[nghb[i]] = current;
					g_val[nghb[i]] = cost;
					f_val[nghb[i]] = cost + h(nghb[i],B,graph_w,graph_h);

					/* if the neighbor was not in the open set yet,
					 * add it, taking in consideration the heuristic
					 * result reported in f */
					if (! dllist_in(openset,nghb[i]))
						dllist_push_prior(openset,f_val,nghb[i],dist_lower);
				}
			}
		}
		// dllist_print(openset);
	}

	// reversely generate the path via parents
	path_t new_path = path_retrace(parent, A, B);

	// don't forget to free everything, valgrind keeps spying you...
	free(parent);
	free(g_val);
	free(f_val);
	free(closedset);
	dllist_free(openset);

	return new_path;

}
