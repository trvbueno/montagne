#include <stdlib.h>

#include "path.h"

path_t path_create_single (int dest)
{
	path_t p;
	p.size = 1;

	p.path = NULL;
	if ((p.path = malloc(sizeof(int))) == NULL) p.size = 0;
	else
		p.path[0] = dest;

	return p;
}

path_t path_retrace (int parent[], int A, int B)
{
	path_t p;

	int point = B, size = 1;
	// the function just follow the path showed by the parent array
	for (point = B; point != A; point = parent[point]) ++size;

	if ((p.path = malloc((size)*sizeof(int))) != NULL)
	{
		int i = size-1;
		for (point = B; i >= 0; point = parent[point], --i)
		{
			p.path[i] = point;
		}
		
		p.size = size;
		return p;
	}
	else exit (EXIT_FAILURE);
}
