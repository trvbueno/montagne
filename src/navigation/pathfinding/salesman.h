#ifndef SALESMAN_H
#define SALESMAN_H

#include "map/map.h"
#include "map/grid.h"
#include "guide/gps.h"
#include "path.h"
#include "matrix.h"

/* return the graph that links every nodes with distances */
matrix salesman_proxy_graph(map_t *m);

/* the nodes are sorted to shorten the total walked distance */
/* this is not the 100% optimal solution wich is O(n!)-complex */
/* instead the NN (nearest neighbor) heuristic is used, with start */
path_t salesman_greedy_path(matrix prox_graph, int nb_nodes, int start, int *l);

/* applies the previous function to each node to find the shortest NN path */
path_t salesman_shortest_greedy(matrix prox_graph, int nb_nodes);

/* get the conversion table from the above path to grid path */
void salesman_prox_to_grid(map_t *m, path_t psource);

#endif