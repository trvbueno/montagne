#include <stdbool.h>

#include "heuristic.h"
#include "distance.h"
#include "map/grid.h"

/* idea for the heurisitc to use:
 * http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html */

float heurstc_manhattan (int a, int b, int w, int h)
{
	int dx = abs(grid_nodex(a,w,h)-grid_nodex(b,w,h));
	int dy = abs(grid_nodey(a,w,h)-grid_nodey(b,w,h));
	return (D1) * (float)(dx + dy);
}

float heurstc_diagonal (int a, int b, int w, int h)
{
	int dx = abs(grid_nodex(a,w,h)-grid_nodex(b,w,h));
	int dy = abs(grid_nodey(a,w,h)-grid_nodey(b,w,h));
	return ((D1) * (float)(dx+dy) + (float)((dx<dy)?dx:dy) * (D2 - (2.0 * D1)));
}
