#ifndef PATH_H
#define PATH_H

#include "matrix.h"

typedef struct path_s
{
	int *path;
	int size;    // the number of nodes, not of edges
} path_t;

path_t path_create_single (int dest);
// create a single edge path

path_t path_retrace (int parent[], int A, int B);
// reverse-trace the path from A to B by parenting nodes

#endif