#ifndef ASTAR_H
#define ASTAR_H

#include "matrix.h"
#include "path.h"

/* A cool benchmark of A*, dijkstra and others in javascript:
 * http://qiao.github.io/PathFinding.js/visual/ */

path_t astar_path(matrix adj_mat, int graph_w, int graph_h, int A, int B);
/* the astar implementation has the same type as the dijkstra's one, as such one
 * could use equally both algorithms, though A* is faster in theory */

#endif
