#ifndef HAPTIC_H
#define HAPTIC_H

void haptic_clear();
void haptic_update (float angle, float distance);
void haptic_print();
unsigned char haptic_get_left();
unsigned char haptic_get_right();
unsigned char haptic_get_forward();

#endif