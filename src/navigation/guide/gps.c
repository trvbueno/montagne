#include "gps.h"

#include <SFML/System/Thread.h>
#include <SFML/System/Sleep.h>
#include <SFML/System/Time.h>
#include <math.h>

#include "map/grid.h"
#include "pathfinding/astar.h"
#include "pathfinding/salesman.h"
#include "audio.h"
#include "haptic.h"
#include "configuration.h"

typedef struct gps_context_s
{
	pathtype_t ptype;	   /* mode to run */

	map_t *map;

	matrix adj_grid;       /* the graph for the grid */
	matrix adj_objects;    /* the graph of all objects */
	int adjW;
	int adjH;

	bool reversed;

	int destination;
	path_t road;
	path_t glob;

	int current_node;
	int current_step;
	float line_distance;   /* the distance for a set of lined up points */
	float needed_angle;    /* the rotation needed for the person face target */

} gps_context_t;

static gps_context_t gps_ctx;
static bool gps_running = false;
static sfThread *gps_thread;

/* functions to iterate over the path */
static inline int increase(int x) {return x+1;}
static inline int decrease(int x) {return x-1;}

bool gps_is_running()
{
	return gps_running;
}

void gps_run()
{
	/* the threshold we consider the limit to stay 'on the road' */
	const float offset_margin = GPS_OFFSET_MARGIN;
	int guynode;

   /* the different ways to follow the path */
	int (*iterate)(int) = (gps_ctx.reversed) ? decrease : increase;

	/* the step describes at wich step of the global path we are */
	/* usually there is only one, but in case of a travelling salesman pbm */
	/* the program has to go from A to B, then B to C, et caetera... */
	int step = (gps_ctx.reversed) ? gps_ctx.glob.size -1 : 0;
	int goal = (gps_ctx.reversed) ? -1 : gps_ctx.glob.size;

	/* location_t current_loc = location_stdin(); */
	location_t current_loc = location_get();

	guynode = grid_closest_node_failsafe (gps_ctx.map, gps_ctx.adj_grid, gps_ctx.adjW, gps_ctx.adjH, current_loc.pos);

	/* compute the path for the first step */
	gps_ctx.road = astar_path(gps_ctx.adj_grid, gps_ctx.adjW, gps_ctx.adjH, guynode, gps_ctx.glob.path[step]);

	// audio_tts_play("Démarrage de la navigation.");

	while(gps_running && ((gps_ctx.reversed && (step > goal)) || ((! gps_ctx.reversed) && (step < goal)) ))
	{
		sfSleep(sfMilliseconds(CPU_WAIT_TIME_MSECS));	/* let the CPU breathe a bit */

		gps_ctx.current_step = step;
		printf("[GPS] STEP %d (dest = %d):", step, gps_ctx.destination);
		printf("  current: %d\n", guynode);

		if (gps_follow_path(offset_margin))  /* follow the path */
		{
			/* and compute a new path if needed (ravelling salesman) */
			if ((gps_ctx.reversed && (step > goal+1)) || ((! gps_ctx.reversed) && (step < goal -1)))
			{
				puts("[GPS] New destination : computing path");
				free(gps_ctx.road.path);
				gps_ctx.road = astar_path(gps_ctx.adj_grid, gps_ctx.adjW, gps_ctx.adjH, gps_ctx.glob.path[step], gps_ctx.glob.path[iterate(step)]);
			}

			/* go to the next step */
			step = iterate(step);

			/* P A U S E */
			// continue;
		}
		else /* the path must be refreshed */
		{
			puts("[GPS] Too far from the way, recomputing path");
			/* refresh the new position of the person */
			/* current_loc = location_stdin(); */
			current_loc = location_get();			
			guynode = grid_closest_node_failsafe (gps_ctx.map, gps_ctx.adj_grid, gps_ctx.adjW, gps_ctx.adjH, current_loc.pos);

			printf("[GPS] Now going to %d\n", guynode);
			/* and go ahead for a new path */
			free(gps_ctx.road.path);
			gps_ctx.road = astar_path(gps_ctx.adj_grid, gps_ctx.adjW, gps_ctx.adjH, guynode, gps_ctx.glob.path[step]);
		}

	}

	if (gps_ctx.road.path) free(gps_ctx.road.path);
	if (gps_ctx.glob.path) free(gps_ctx.glob.path);

	if (gps_running) puts("[GPS] Destination reached");
	gps_running = false;
}

bool gps_follow_path (const float off_margin)
{
	/* loop conditions */
	bool arrived = false, on_the_road = true;
	float criteria = 1.f/(3.f*(float)(gps_ctx.map->m_unit));
	float d = 0.f;

	/* node_index represents the node we are at currently, roughly */
	int node_index = 0, node;

	/* the angle if to be considered from the person referential, forward = 0 */
	float angle = 0.f;
	float distance = 0.f;

	point_t final;
	grid_node_coord(gps_ctx.map, gps_ctx.road.path[gps_ctx.road.size-1], gps_ctx.adjW, gps_ctx.adjH, &(final.x), &(final.y));

	/* the point we come from, the point we go to */
	point_t from_pt, to_pt;

	location_t current_loc = location_get();

	while ((! arrived) && on_the_road)
	{
		node = gps_ctx.road.path[node_index];
		grid_node_coord(gps_ctx.map, node, gps_ctx.adjW, gps_ctx.adjH, &(from_pt.x), &(from_pt.y));

		/* compute the next coordinates to go */
		node = gps_ctx.road.path[node_index+1];
		grid_node_coord(gps_ctx.map, node, gps_ctx.adjW, gps_ctx.adjH, &(to_pt.x), &(to_pt.y));

		current_loc = location_get();

		/* check if the person has passed a new point                    */
		/* if the from_pt and current_pos are the same side from to_pt, */
		/* the program consider the person still has to reach to_pt    */
		while ((node_index < gps_ctx.road.size-1) &&
			(((to_pt.x - from_pt.x)*(to_pt.x - current_loc.pos.x) < 0) || ((to_pt.y - from_pt.y)*(to_pt.y - current_loc.pos.y) < 0) || (
			(fabsf(to_pt.x - current_loc.pos.x) < criteria) && (fabsf(to_pt.y - current_loc.pos.y) < criteria))))
		{
			++ node_index;
			from_pt = to_pt;
			node = gps_ctx.road.path[node_index+1];
			grid_node_coord(gps_ctx.map, node, gps_ctx.adjW, gps_ctx.adjH, &(to_pt.x), &(to_pt.y));
		}

		/* compute the distance */
		distance = map_distance_A_to_B(final, current_loc.pos);

		/* and determinate which direction we should go */
		angle = gps_best_rotation(current_loc.angle, map_angle_A_to_B(current_loc.pos, to_pt));

		/* updates haptic engine */
		haptic_update(angle, distance);

		/* update the context */
		gps_ctx.needed_angle = angle;
		gps_ctx.line_distance = distance;
		gps_ctx.current_node = gps_ctx.road.path[node_index];

		/* and get the new location of the person */
		current_loc = location_get();

		/* check if the peson has reached the destination or not */
		on_the_road = (map_distance_A_to_B(to_pt, current_loc.pos) <= off_margin);
		arrived = (node_index == gps_ctx.road.size-1);
	}

	return arrived;
}

float gps_best_rotation (float alpha, float beta)
{
	/* ]-180, 180] -> [0, 360[ */
	alpha += 180;
	beta += 180;

	float diff = beta - alpha;
	if (diff >= 180) diff = 360 - diff;

	return diff; 
}

/* This is an attempt to make better approximations of distances in a path
 * Actually it does not work.
 *
float gps_dist_to_next_change (int start_node)
{
	float distance = 1.f/((float)gps_ctx.map->m_unit);
	int segments = 0;          // the number of lined up segements

	// compute the offset X/Y from start to next as a reference
	int refx = grid_nodex(gps_ctx.road.path[start_node+1], gps_ctx.adjW, gps_ctx.adjH) - grid_nodex(gps_ctx.road.path[start_node], gps_ctx.adjW, gps_ctx.adjH);
	int refy = grid_nodey(gps_ctx.road.path[start_node+1], gps_ctx.adjW, gps_ctx.adjH) - grid_nodey(gps_ctx.road.path[start_node], gps_ctx.adjW, gps_ctx.adjH);
	int offx = refx, offy = refy;

	// printf("(%d, %d)\n", refx, refy);

	// and while it's not end, and the offset are same as reference, go on
	while (((start_node + segments + 1) < gps_ctx.road.size) && (offx == refx) && (offy == refy))
	{
		offx = grid_nodex(gps_ctx.road.path[start_node+segments+1], gps_ctx.adjW, gps_ctx.adjH) - grid_nodex(gps_ctx.road.path[start_node+segments], gps_ctx.adjW, gps_ctx.adjH);
		offy = grid_nodey(gps_ctx.road.path[start_node+segments+1], gps_ctx.adjW, gps_ctx.adjH) - grid_nodey(gps_ctx.road.path[start_node+segments], gps_ctx.adjW, gps_ctx.adjH);
		// printf("L(%d, %d)\n", offx, offy);
		++ segments;
	}

	return segments*distance;
}
*/

bool gps_init()
{
	gps_thread =  sfThread_create(&gps_run, NULL);	
	gps_ctx.map = map_get_global(); /* map must be initialized ! */

	if(!gps_thread || !gps_ctx.map) return false;

	gps_ctx.ptype = SHORTEST;
	gps_running = false;

	/* pre-computing grid */
	grid_gridify(gps_ctx.map, &(gps_ctx.adj_grid), &(gps_ctx.adjW), &(gps_ctx.adjH));
	gps_ctx.adj_objects = NULL;
}

bool gps_set_parameters(point_t p, pathtype_t mode, int opti, bool reversed)
{
	if(gps_running) return false;

	gps_ctx.ptype = mode;

	/* if the mode is optimized the program musts compute the proximity graph */
	/* to be able to determine wich way is the best */
	if (mode == OPTIMIZED)
	{
		gps_ctx.reversed = reversed;
		gps_ctx.destination = opti;
		gps_ctx.adj_objects = salesman_proxy_graph(gps_ctx.map);
	}
	/* else just compute a single path */
	else
	{
		gps_ctx.destination = grid_closest_node_failsafe(gps_ctx.map, gps_ctx.adj_grid, gps_ctx.adjW, gps_ctx.adjH, p);
	}

	return true;
}

path_t gps_get_path()
{
	return gps_ctx.road;
}

int gps_get_adjW()
{
	return gps_ctx.adjW;
}

int gps_get_adjH()
{
	return gps_ctx.adjH;
}

matrix gps_get_adjgrid (void)
{
	return gps_ctx.adj_grid;
}

matrix gps_get_adjprox (void)
{
	return gps_ctx.adj_grid;
}

float gps_get_angle (void)
{
	return gps_ctx.needed_angle;
}

float gps_get_distance (void)
{
	return gps_ctx.line_distance;
}

int gps_get_curr_node (void)
{
	return gps_ctx.current_node;
}

int gps_get_curr_step (void)
{
	return gps_ctx.current_step;
}

bool gps_start()
{
	int plouf = 0;
	if(gps_running) return false;

	/* computing the path */
	if (gps_ctx.ptype == SHORTEST)
		gps_ctx.glob = path_create_single(gps_ctx.destination);

	else if (gps_ctx.ptype == OPTIMIZED)
	{
		/* get the sorted list of node to pass by */
		gps_ctx.glob = salesman_greedy_path(gps_ctx.adj_objects, gps_ctx.map->obj_count, gps_ctx.destination, &plouf);
		/* and convert them to the grid system */
		salesman_prox_to_grid(gps_ctx.map, gps_ctx.glob);
	}
	for (int i = 0; i < gps_ctx.glob.size; ++i) printf("%d ",gps_ctx.glob.path[i]);
	putchar('\n');

	gps_running = true;
	sfThread_launch(gps_thread);

	return true;
}

bool gps_pause()
{
	/* not implemented yet */
}

bool gps_stop()
{
	if(!gps_running) return false;

	gps_running = false;
	sfThread_terminate(gps_thread);
	free(gps_ctx.road.path);
	gps_ctx.road.path = NULL;
	gps_ctx.road.size = 0;
}