#ifndef GPS_H
#define GPS_H

#include "matrix.h"
#include "map/map.h"
#include "map/location.h"
#include "pathfinding/path.h"

#include <stdbool.h>

/* -------------------------------- M U L T I - T H R E A D    C O N T E X T */
typedef enum pathtype_e {SHORTEST, OPTIMIZED} pathtype_t;


/* ------------------------------------------ " G P S "    F U N C T I O N S */

/* finite loop to make the person progress through the path until (s)he */
/* reaches the ending point, or (s)he goes out of the limits of margin */
/* the function returns TRUE if the ending point was reached or FALSE */
bool gps_follow_path (const float off_margin);

/* return the ideal rotation to go rotate from alpha ° to beta °   */
/* take angles from ]-180, 180]                                   */
float gps_best_rotation(float alpha, float beta);

/* return the maximum distance that to be on a straight line path portion */
float gps_dist_to_next_change (int start_node);

/* set up the context */
bool gps_init();

bool gps_set_parameters(point_t p, pathtype_t mode, int opti_end, bool reversed);

path_t gps_get_path();

int gps_get_adjW();

int gps_get_adjH();

matrix gps_get_adjgrid (void);

matrix gps_get_adjprox (void);

float gps_get_angle (void);

float gps_get_distance (void);

int gps_get_curr_node (void);

int gps_get_curr_step (void);

bool gps_start();

bool gps_pause();

bool gps_stop();

bool gps_is_running();

#endif