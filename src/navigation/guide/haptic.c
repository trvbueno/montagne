#include <stdio.h>
#include <math.h>

#include "haptic.h"

static unsigned char haptic_left = 0;
static unsigned char haptic_right = 0;
static unsigned char haptic_forward = 0;

unsigned char distance_to_signal (float x)
{
	/* this function f: lim f [x->0.5+]s = 255- */
	if (x >= 0.5) return (char)(420.f*expf(-x));
	else if (x >= 0.1) return 255;
	else return 0;
}

unsigned char haptic_get_left()
{
	return haptic_left;
}

unsigned char haptic_get_right()
{
	return haptic_right;	
}

unsigned char haptic_get_forward()
{
	return haptic_forward;	
}

void haptic_clear()
{
	haptic_left = 0;
	haptic_right = 0;
	haptic_forward = 0;
}

void haptic_update (float angle, float distance)
{
	const int haptic_epsilon = 0.01;

	/* set the intensity in function of distance */
	haptic_forward = distance_to_signal(distance);
	
	if ((angle > -haptic_epsilon) && (angle < haptic_epsilon))
	{
		haptic_left = 0;
		haptic_right = 0;
	}
	else if (angle >= haptic_epsilon)
	{
		/* ensure the result will fit in 0-255 */
		angle = (angle > 90 - haptic_epsilon) ? 90.f : angle;
		/* and process */
		haptic_left = (unsigned char) ((angle*255.f)/90.f);
		haptic_right = 0;
		}
	else
	{
		/* ensure the result will fit in 0-255 */
		angle = (angle < -90 + haptic_epsilon) ? -90.f : angle;
		/* and process */  
		haptic_left = 0;
		haptic_right = (unsigned char) ((-angle*255.f)/90.f);
	}
}

void haptic_print ()
{
	printf("[HAPTIC] %ud <- %ud -> %ud\n", haptic_left, haptic_forward, haptic_right);
}