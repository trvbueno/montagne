#include "player.h"

static sfSound *track;

bool player_init()
{
	track = sfSound_create();

	return (track != NULL);
}

void player_play(sfSoundBuffer *sound)
{
	sfSound_stop(track);
	sfSound_setBuffer(track, sound);
	sfSound_play(track);
}

void player_play_blocking(sfSoundBuffer *sound)
{
	player_play(sound);
	sfSoundStatus stat = sfPlaying;
	while(stat == sfPlaying)
	{
		stat = sfSound_getStatus(track);
		sfSleep(sfMilliseconds(50));
	}
}

void player_pause()
{
	sfSound_pause(track);
}

void player_stop()
{
	sfSound_stop(track);
}