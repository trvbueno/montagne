#ifndef INTERFACE_H
#define INTERFACE_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "menu.h"
#include "audio.h"

bool interface_init();
bool interface_init_audio_cache();
void interface_read();
void interface_help();
void interface_read_caption();
void interface_scroll_up();
void interface_scroll_down();
void interface_select();

#endif