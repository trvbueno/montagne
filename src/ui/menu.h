#ifndef MENU_H
#define MENU_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_SUBMENUS 10

typedef struct menu_s *menu;

menu menu_init(const char *name, const char *help, void (*action)(menu));
bool menu_add(menu m, menu subm);
const char* menu_get_name(menu m);
const char* menu_get_help(menu m);
bool menu_enabled(menu m);
void menu_toggle(menu m);
void menu_activate(menu m);
int menu_getsubs_count(menu m);
menu* menu_getsubs(menu m);

#endif