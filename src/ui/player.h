#ifndef PLAYER_H
#define PLAYER_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <SFML/Audio/Sound.h>
#include <SFML/Audio/SoundBuffer.h>
#include <SFML/Audio/SoundStatus.h>
#include <SFML/System/Sleep.h>

bool player_init();
void player_play(sfSoundBuffer *sound);
void player_play_blocking(sfSoundBuffer *sound);
void player_pause();
void player_stop();

#endif