#include "audio.h"

#include <SFML/Audio/SoundBuffer.h>
#include <SFML/Audio/SoundStatus.h>
#include <SFML/System/Mutex.h>

#include <espeak/speak_lib.h>

#include "player.h"
#include "configuration.h"

static short *tts_buffer;
static int tts_buffer_size;
static sfSoundBuffer *tts_sound;
static bool audio_play_block;

/* the audio engine can be accessed through multiple threads, and as it is designed to be unique and state-based, the play and cache functions are protected with mutexes */ 
sfMutex *audio_mutex;
sfMutex *audio_mutex_cache;

int audio_tts_gen_callback(short *samples, int samples_count, espeak_EVENT *tts_events)
{
	if(samples_count != 0)
	{
		tts_buffer = realloc(tts_buffer, (tts_buffer_size + samples_count) * sizeof(short));
		memcpy(&tts_buffer[tts_buffer_size], samples, samples_count * sizeof(short));
		tts_buffer_size += samples_count;

		// TODO : if no more memory is available to store these samples, we need to properly crash
	}

	/* if espeak has rendered every word in the sentence, we generate the soundbuffer with the samples */
	if(tts_events[0].type == espeakEVENT_LIST_TERMINATED)
		tts_sound = sfSoundBuffer_createFromSamples(tts_buffer, tts_buffer_size, 1, 22050);

	return 0;
}

bool audio_init()
{
	audio_cache = hashmap_new();
	audio_mutex = sfMutex_create();
	audio_mutex_cache = sfMutex_create();

	if(!audio_cache || !audio_mutex || !audio_mutex_cache) return false;

	tts_buffer = NULL;
	tts_buffer_size = 0;
	tts_sound = NULL;
	audio_play_block = false;

	if(!player_init()) return false;

	if(!audio_cache) return false;
	if(!audio_mutex) return false;
	if(!audio_mutex_cache) return false;

	espeak_Initialize(AUDIO_OUTPUT_SYNCHRONOUS, 0, NULL, 0);
	espeak_SetSynthCallback(&audio_tts_gen_callback);
	espeak_SetVoiceByName(AUDIO_TTS_VOICE);
	espeak_SetParameter(espeakRATE, AUDIO_TTS_RATE, 0);
	espeak_SetParameter(espeakPITCH, AUTIO_TTS_PITCH, 0);

	return true;
}

sfSoundBuffer *audio_tts_gen(const char *str)
{
	if(!tts_buffer) free(tts_buffer);
	if(!tts_sound)
	{
		sfSoundBuffer_destroy(tts_sound);
		tts_sound = NULL;
	}
	tts_buffer_size = 0;

	unsigned int flags = espeakCHARS_AUTO | espeakENDPAUSE;
	espeak_Synth(str, strlen(str), 0, POS_CHARACTER, 0, flags, NULL, NULL);
	espeak_Synchronize();

	return tts_sound;
}

void audio_tts_cache(const char *str)
{
	sfMutex_lock(audio_mutex_cache);

	if(!strcmp(str, "")) return;

	sfSoundBuffer *tts_snd = NULL;
	char *tempstr = malloc(strlen(str) + 1);
	strcpy(tempstr, str);

	if(hashmap_get(audio_cache, tempstr, (void**)(&tts_snd)) == MAP_MISSING)
	{
		tts_snd = sfSoundBuffer_copy(audio_tts_gen(str));

		hashmap_put(audio_cache, tempstr, tts_snd);

		printf("[AUDIO] '%s' cached.\n", tempstr);
	}
	else printf("[AUDIO] '%s' already in cache.\n", tempstr);

	sfMutex_unlock(audio_mutex_cache);
}

void audio_block()
{
	audio_play_block = true;
}

void audio_tts_play(const char *str)
{
	sfMutex_lock(audio_mutex);

	if(!strcmp(str, ""))
	{
		sfMutex_unlock(audio_mutex);
		return;
	}

	sfSoundBuffer *tts_snd;
	char *tempstr = malloc(strlen(str) + 1);
	strcpy(tempstr, str);

	if(hashmap_get(audio_cache, tempstr, (void**)(&tts_snd)) == MAP_MISSING)
	{
		audio_tts_cache(str);
		hashmap_get(audio_cache, tempstr, (void**)(&tts_snd));
	}

	if(tts_snd)
	{
		printf("[AUDIO] playing '%s'.\n", tempstr);
		if(!audio_play_block)
			player_play(tts_snd);
		else
		{
			player_play_blocking(tts_snd);
			audio_play_block = false;
		}
	}

	sfMutex_unlock(audio_mutex);
}