#include "menu.h"

struct menu_s {
	char *name;
	char *help;
	bool enabled;
	void (*action)(menu);
	int subsc;
	menu *subs;
};

menu menu_init(const char *name, const char *help, void (*action)(menu))
{
	menu m = malloc(sizeof(struct menu_s));
	m->name = malloc(strlen(name)+1);
	m->help = malloc(strlen(help)+1);
	strcpy(m->name, name);
	strcpy(m->help, help);
	m->enabled = true;
	m->action = action;
	m->subsc = 0;
	m->subs = malloc(sizeof(menu) * MAX_SUBMENUS);
	return m;
}

bool menu_add(menu m, menu subm)
{
	if(m->subsc < MAX_SUBMENUS)
	{
		m->subs[m->subsc] = subm;
		m->subsc++;
		return true;
	}

	return false;
}

const char* menu_get_name(menu m)
{
	return m->name;
}

const char* menu_get_help(menu m)
{
	return m->help;
}

bool menu_enabled(menu m)
{
	return m->enabled;
}

void menu_toggle(menu m)
{
	m->enabled = !(m->enabled);
}

void menu_activate(menu m)
{
	if(m->action)
		m->action(m);
}

int menu_getsubs_count(menu m)
{
	return m->subsc;
}

menu* menu_getsubs(menu m)
{
	return m->subs;
}