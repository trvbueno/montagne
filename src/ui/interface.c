#include "interface.h"

#include "handlers/menu_map.h"
#include "handlers/menu_nav.h"

static menu main_ui;
static menu current_root;
static menu current_item;
static int last_sub;

void interface_set_home()
{
	current_root = main_ui;
	current_item = menu_getsubs(main_ui)[0];
	last_sub = 0;
}

bool interface_init()
{
	main_ui = menu_init("Menu principal", "", NULL);

	menu_add(main_ui, menu_init("Carte", "Repérage des points d'intérêt dans la carte.", NULL));
	menu map = menu_getsubs(main_ui)[0];

	menu_add(map, menu_init("Normal", "Points d'intérêt non triés.", &handle_map_select_normal));
	/* possible upgrade : implement a generic sort function, but there is no easy solution to physically sort entries */
	// menu_add(map, menu_init("Catégorie", "Points d'interêt classés par catégorie.", NULL));
	// menu_add(map, menu_init("Alphabétique", "Points d'interêt classés par ordre alphabétique.", NULL));
	// menu_add(map, menu_init("Proximité", "Points d'interêt classés par proximité géographique.", NULL));

	menu_add(main_ui, menu_init("Navigation", "Gestion et programmation d'un itinéraire.", NULL));
	menu nav = menu_getsubs(main_ui)[1];

	menu_add(nav, menu_init("En cours", "Gestion de la navigation actuellement en fonctionnement.", NULL));
	menu nav_current = menu_getsubs(nav)[0];

	menu_add(nav_current, menu_init("Informations", "Informations sur la navigation actuelle.", &handle_nav_current_infos));
	menu_add(nav_current, menu_init("Arrêter", "Arrêter la navigation actuelle.", &handle_nav_current_stop));

	menu_add(nav, menu_init("Nouvelle", "Création d'une nouvelle navigation.", NULL));
	menu nav_new = menu_getsubs(nav)[1];

	menu_add(nav_new, menu_init("Mode direct", "Mode de navigation direct vers un point d'intérêt.", &handle_nav_new_direct));
	menu_add(nav_new, menu_init("Mode optimisé", "Mode de visite des points d'intêrêt présents sur un trajet optimisé vers une destination.", &handle_nav_new_optimized));

	/* possible upgrades : implement a configuration and a help menu */
	menu_add(main_ui, menu_init("Préférences", "Réglages du système.", NULL));
	menu conf = menu_getsubs(main_ui)[2];

	menu_add(main_ui, menu_init("Aide", "Informations pratique sur l'utilisation du système.", NULL));
	menu help = menu_getsubs(main_ui)[3];

	interface_set_home();
	interface_init_audio_cache();

	return true;
}

/* pre-cache audio from the menus at program startup */
void interface_recursive_audio_cache(menu m)
{
	if(m)
	{
		audio_tts_cache(menu_get_name(m));
		audio_tts_cache(menu_get_help(m));

		for(int i=0; i<menu_getsubs_count(m); i++)
			interface_recursive_audio_cache(menu_getsubs(m)[i]);
	}
}

bool interface_init_audio_cache()
{
	interface_recursive_audio_cache(main_ui);
}

void interface_read()
{
	audio_tts_play(menu_get_name(current_item));
}

void interface_read_caption()
{
	audio_tts_play(menu_get_help(current_item));
}

void interface_help()
{
	audio_tts_play(menu_get_help(current_item));
}

void interface_scroll_up()
{
	last_sub--;
	if(last_sub == -1) last_sub =  menu_getsubs_count(current_root) - 1;

	current_item = menu_getsubs(current_root)[last_sub];

	if(!menu_enabled(current_item)) interface_scroll_up();
}

void interface_scroll_down()
{
	last_sub++;
	if(last_sub == menu_getsubs_count(current_root)) last_sub = 0;

	current_item = menu_getsubs(current_root)[last_sub];

	if(!menu_enabled(current_item)) interface_scroll_down();
}

void interface_select()
{
	menu m = current_item;

	if(menu_enabled(current_item))
	{
		menu_activate(m);
		
		if(menu_getsubs_count(current_item) > 0)
		{
			current_root = current_item;
			last_sub = 0;
			current_item = menu_getsubs(current_root)[last_sub];

			if(!menu_enabled(current_item)) interface_scroll_down();
		}
		else
		{
			audio_block();
			audio_tts_play("Retour au menu principal");
			interface_set_home();
		}
	}
}