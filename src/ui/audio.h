#ifndef AUDIO_H
#define AUDIO_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hashmap.h"

#define AUDIO_TEMPO 110

static hmap_t audio_cache;

bool audio_init();
void audio_tts_cache(const char *str);
void audio_tts_play(const char *str);
void audio_block();

#endif