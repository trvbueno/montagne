#ifndef MENU_MAP
#define MENU_MAP

#include "menu.h"

void handle_map_select_normal(menu m);
void handle_map_select_normal_callback(menu m, void (*callback)());
void handle_map_toggle_item(menu m);
int menu_map_get_selected();

#endif