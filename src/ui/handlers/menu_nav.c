#include "menu_nav.h"

#include "menu_map.h"
#include "audio.h"
#include "guide/gps.h"
#include "map/map.h"
#include "map/grid.h"

void no_nav_currently()
{
	audio_block();
	audio_tts_play("Pas de navigation actuellement en cours.");	
}

void handle_nav_current_infos(menu m)
{
	if(gps_is_running())
	{
		audio_tts_play("Informations sur la navigation courante.");

		char txt_buf[256];
		sprintf(txt_buf, "Distance à parcourir jusqu'à la destination suivante : %.1f mètres.", gps_get_distance());
		audio_block();
		audio_tts_play(txt_buf);

		/* salesman path */
		if(gps_get_curr_step() > 0)
		{
			sprintf(txt_buf, "Nombre de points de navigation à atteindre avant la destination finale : %d.", gps_get_curr_step());
			audio_block();
			audio_tts_play(txt_buf);
		}
	}
	else no_nav_currently();
}

void handle_nav_current_stop(menu m)
{
	if(gps_is_running())
	{
		gps_stop();
		audio_block();
		audio_tts_play("Arrêt de la navigation courante.");	
	}
	else no_nav_currently();
}

void handle_nav_new(menu m)
{
	if(gps_is_running())	handle_nav_current_stop(m);

	audio_block();
	audio_tts_play("Création d'un trajet, veuillez sélectionner la destination finale:");	
}

void handle_nav_new_direct(menu m)
{
	handle_nav_new(m);
	handle_map_select_normal_callback(m, &handle_nav_new_direct_selection_made);
}

void handle_nav_new_optimized(menu m)
{
	handle_nav_new(m);
	handle_map_select_normal_callback(m, &handle_nav_new_optimized_selection_made);
}

void handle_nav_new_direct_selection_made(menu m)
{
	audio_block();
	audio_tts_play("Trajet direct programmé.");

	map_t *map = map_get_global();
	printf("[NAV] direct vers : %s, noeuds : %d -> %d\n",
		map->obj[menu_map_get_selected()].name,
		grid_closest_node(map, location_get().pos),
		grid_closest_node(map, map->obj[menu_map_get_selected()].door));	

	gps_set_parameters(map->obj[menu_map_get_selected()].door, SHORTEST, -1, 0);

	gps_start();
}

void handle_nav_new_optimized_selection_made(menu m)
{
	audio_block();
	audio_tts_play("Trajet optimisé programmé.");

	map_t *map = map_get_global();
	printf("[NAV] opt vers : %s, noeuds : %d -> %d\n", 
		map->obj[menu_map_get_selected()].name,
		grid_closest_node(map, location_get().pos),
		grid_closest_node(map, map->obj[menu_map_get_selected()].door));	

	gps_set_parameters(location_get().pos, OPTIMIZED, menu_map_get_selected(), 1);

	gps_start();
}