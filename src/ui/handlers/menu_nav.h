#ifndef MENU_NAV
#define MENU_NAV

#include "menu.h"

void handle_nav_current_infos(menu m);
void handle_nav_current_stop(menu m);
void handle_nav_new_direct(menu m);
void handle_nav_new_optimized(menu m);
void handle_nav_new_direct_selection_made(menu m);
void handle_nav_new_optimized_selection_made(menu m);

#endif