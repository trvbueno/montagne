#include "menu_map.h"

#include "map/map.h"
#include "map/grid.h"

#include <stdio.h>

/* menu_map is basically a "listbox" component of the UI, emulated in a C-way */

static int menu_map_id_selected;
static void (*selection_callback)();

/* builds if needed the submenus composing the map list */
void handle_map_select_normal(menu m)
{
	map_t *map = map_get_global();
	menu_map_id_selected = -1;
	selection_callback = NULL;

	if(menu_getsubs_count(m) != map->obj_count)
	{
		for(int i=0; i<map->obj_count; i++)
		{
			menu_add(m, menu_init(map->obj[i].name, map->obj[i].description, &handle_map_toggle_item));
			printf("%s : %d\n", map->obj[i].name, grid_closest_node(map, map->obj[i].door));	
		}
	}
}

void handle_map_select_normal_callback(menu m, void (*callback)())
{
	handle_map_select_normal(m);
	selection_callback = callback;
}

void handle_map_toggle_item(menu m)
{
	map_t *map = map_get_global();
	for(int i=0; i<map->obj_count; i++)
	{
		if(!strcmp(menu_get_name(m), map->obj[i].name))
		{
			menu_map_id_selected = i;
			break;
		}
	}

	if(selection_callback != NULL)	selection_callback();
}

int menu_map_get_selected()
{
	return menu_map_id_selected;
}