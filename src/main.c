#include <stdio.h>

#include <SFML/Window.h>
#include <SFML/System.h>

#include "interface.h"
#include "audio.h"
#include "map/map.h"
#include "guide/gps.h"
#include "input.h"
#include "graphics.h"
#include "configuration.h"

int main(int argc, const char* argv[])
{
	/* Audio engine initialization
	 * provides TTS and audio cache
	 */
	if(!audio_init())
	{
		printf("Audio initialization failed.\n");
		return -1;
	}

	/* Interface initialization
	 * provides user interface
	 */
	if(!interface_init())
	{
		printf("Interface initialization failed.\n");
		return -1;
	}

	/* Map initialization
	 * provides geo data
	 */
	if(!map_init())
	{
		printf("Map initialization failed.\n");
		return -1;
	}

	/* GPS initialization
	 * provides guiding
	 */
	if(!gps_init())
	{
		printf("GPS initialization failed.\n");
		return -1;
	}

	/* Input initialization
	 * provides keystrokes
	 */
	if(!input_init())
	{
		printf("Input initialization failed.\n");
		return -1;
	}

	/* Graphics initialization
	 * provides a graphical debug UI, if enabled
	 */
	if(GRAPHICS_DEBUG_ENABLED == 1)
	{		
		if(!graphics_init())
		{
			printf("Graphics initialization failed.\n");
			return -1;
		}
	}

	/* Main loop
	 * provides event to the interface
	 */
	sfKeyCode key;
	while(1)
	{
		key = input_get_key();	/* blocking call */
		switch(key)
		{
			/* key up -> scroll menu up */
			case sfKeyUp:
				interface_scroll_up();
				break;

			/* key down -> scroll menu down */
			case sfKeyDown:
				interface_scroll_down();
				break;

			/* key left -> read menu caption */
			case sfKeyLeft:
				audio_block();
				interface_read_caption();
				continue; /* prevents menu name to be read twice */
				break;

			/* key left -> select menu item */
			case sfKeyRight:
				interface_select();
				break;

			default:
				break;
		}

		interface_read();
	}

	return 0;
}