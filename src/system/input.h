#ifndef INPUT_H
#define INPUT_H

#include <SFML/Window/Keyboard.h>

#include <stdbool.h>

bool input_init();
sfKeyCode input_get_key();

#endif