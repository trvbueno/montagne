#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#define MAP_ACCESS_PATH "../res/supertest.yaml"

#define GRAPHICS_FONT_ACCESS_PATH "../res/DejaVuSansMono.ttf"
#define GRAPHICS_DEBUG_ENABLED 1

#define INPUT_COOLDOWN_MSECS 500

#define CPU_WAIT_TIME_MSECS 100

#define AUDIO_TTS_VOICE "mb-fr1"
#define AUDIO_TTS_RATE 105
#define AUTIO_TTS_PITCH 50

#define GPS_OFFSET_MARGIN 0.35f

#endif