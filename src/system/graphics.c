#include "graphics.h"

#include "guide/gps.h"
#include "map/map.h"
#include "matrix.h"
#include "map/grid.h"
#include "pathfinding/path.h"
#include "map/location.h"
#include "guide/haptic.h"
#include "configuration.h"

#include <SFML/Window.h>
#include <SFML/Graphics.h>
#include <SFML/System.h>

#include <math.h>

/* now this is getting dirty */
#ifdef __unix__ 
	#include <X11/Xlib.h>
#endif

/* General window-related objects */
static sfThread *graphics_thread;
static sfRenderWindow *graphics_window;
static sfContext *graphics_context;

/* Graphics objects */
static sfVertexArray *graphics_shapes;
static sfVertexArray *graphics_grid;
static sfVertexArray *graphics_path;
static sfCircleShape *graphics_mouse_cursor;
static sfCircleShape *graphics_node_cursor;
static sfVertexArray *graphics_mouse_angle;
static sfVertexArray *graphics_gps_angle;
static sfFont 		 *graphics_font;
static sfText 		 *graphics_coords_txt;
static sfText 		 *graphics_gps_txt;
static sfRectangleShape	*graphics_haptic_left;
static sfRectangleShape	*graphics_haptic_right;
static sfRectangleShape	*graphics_haptic_forw;

/* Data */
static int	mouse_angle = 0;

static int W = 700;
static int H = 700;

void graphics_update_mouse_angle(float a)
{
	float rads = a*M_PI/180;

	sfVector2f cpos = sfCircleShape_getPosition(graphics_mouse_cursor);

	sfVertexArray_clear(graphics_mouse_angle);

	sfVertex angle_origin;
	angle_origin.position.x = cpos.x + 4;
	angle_origin.position.y = cpos.y + 4;
	angle_origin.color = sfCyan;

	sfVertex angle_dest;
	angle_dest.position.x = (cpos.x + 4)+(cos(rads)*20);
	angle_dest.position.y = (cpos.y + 4)+(sin(rads)*20);
	angle_dest.color = sfCyan;

	sfVertexArray_append(graphics_mouse_angle, angle_origin);
	sfVertexArray_append(graphics_mouse_angle, angle_dest);

	location_t new_loc = location_get();

	float ang = 360 - a;

	if(ang >= 0.0f && ang <= 180.0f) new_loc.angle = ang;
		else new_loc.angle = ang - 360.0f;

	location_set(new_loc);
}

void graphics_update_gps_angle(float d)
{
	float deg = mouse_angle + (d*-1);
	if(deg > 360) deg = (int)deg % 360;
	if(deg < 0) deg = 360 + deg;

	float rads = deg*M_PI/180;

	sfVector2f cpos = sfCircleShape_getPosition(graphics_mouse_cursor);

	sfVertexArray_clear(graphics_gps_angle);

	sfVertex angle_origin;
	angle_origin.position.x = cpos.x + 4;
	angle_origin.position.y = cpos.y + 4;
	angle_origin.color = sfGreen;

	sfVertex angle_dest;
	angle_dest.position.x = (cpos.x + 4)+(cos(rads)*20);
	angle_dest.position.y = (cpos.y + 4)+(sin(rads)*20);
	angle_dest.color = sfGreen;

	sfVertexArray_append(graphics_gps_angle, angle_origin);
	sfVertexArray_append(graphics_gps_angle, angle_dest);
}

/* Coordinates (x,y) are relative to the render window, to which the origin is the top left corner, as opposed to the map one : the bottom left corner */
void graphics_update_location(float x, float y, float ang)
{
	map_t *plan = map_get_global();
	float scaleW = plan->width / W;
	float scaleH = plan->height / H;

	location_t new_loc;
	new_loc.pos.x = x * scaleH;
	new_loc.pos.y = (H - y) * scaleW;
	new_loc.angle = 0.0f;

	location_set(new_loc);

	sfCircleShape_setPosition(graphics_mouse_cursor, (sfVector2f){ x - 5, y - 5 });

	sfVector2f npos;
	grid_node_coord(plan, grid_closest_node(plan, new_loc.pos), gps_get_adjW(), gps_get_adjH(), &(npos.x), &(npos.y));

	npos.x /= scaleH;
	npos.y /= scaleW;
	npos.y = H - npos.y;

	npos.x -= 3;
	npos.y -= 3;

	sfCircleShape_setPosition(graphics_node_cursor, npos);

	graphics_update_mouse_angle(ang);
}

bool graphics_shapes_init()
{
	/* Precompute map vertex */
	graphics_shapes = sfVertexArray_create();

	if(!graphics_shapes) return false;

	map_t *plan = map_get_global();
	float scale = plan->width / W;
	sfVector2f vpos;
	point_t gpos;
	sfVertex vertex;
	vertex.color.r = 64;
	vertex.color.g = 64;
	vertex.color.b = 64;

	for (int i = 0; i<W; i++)
	{
		for (int j = 0; j<H; j++)
		{

			gpos.x = i*scale;
			gpos.y = plan->height - j*scale;
			vpos.x = i;
			vpos.y = j;

			if (!map_accessible(plan, &gpos))
			{
				vertex.position = vpos;
				sfVertexArray_append(graphics_shapes, vertex);
			}
		}
	}

	return true;
}

bool graphics_grid_init()
{
	graphics_grid = sfVertexArray_create();

	if(!graphics_grid) return false;

	sfVertexArray_setPrimitiveType(graphics_grid, sfLines);

	map_t *plan = map_get_global();
	float scale = plan->width / W;
	sfVector2f vpos;
	point_t gpos;
	sfVertex vertex;
	matrix adjacence = NULL;
	int nodesW, nodesH;
	path_t p, opti;
	point_t gpos2;
	sfVertex lvertex;
	lvertex.color.r = 32;
	lvertex.color.g = 32;
	lvertex.color.b = 32;
	vertex.color.r = 174;
	vertex.color.g = 174;
	vertex.color.b = 174;

	grid_gridify(plan, &adjacence, &nodesW, &nodesH);
	for (int no = 0; no < nodesW*nodesH; ++no) {
		grid_node_coord(plan, no, nodesW, nodesH, &(gpos.x), &(gpos.y));
		for (int l = 0; l < nodesW*nodesH; ++l)
		{
			if (adjacence[matx_index(no, l, nodesW*nodesH, nodesW*nodesH)] > 0.001)
			{
				grid_node_coord(plan, l, nodesW, nodesH, &(gpos2.x), &(gpos2.y));

				vpos.x = (int)((gpos.x)/scale);
				vpos.y = H-1-(int)((gpos.y)/scale);				
				lvertex.position = vpos;
				sfVertexArray_append(graphics_grid, lvertex);

				vertex.position = vpos;
				sfVertexArray_append(graphics_shapes, vertex);

				vpos.x = (int)((gpos2.x)/scale);
				vpos.y = W-1-(int)((gpos2.y)/scale);				
				lvertex.position = vpos;
				sfVertexArray_append(graphics_grid, lvertex);
			}
		}
	}

	return true;
}

bool graphics_path_init()
{
	graphics_path = sfVertexArray_create();

	if(!graphics_path) return false;

	sfVertexArray_setPrimitiveType(graphics_path, sfLines);

	return true;
}

bool graphics_path_update()
{
	sfVertexArray_clear(graphics_path);

	map_t *plan = map_get_global();
	float scale = plan->width / W;
	sfVector2f vpos;
	point_t gpos;
	point_t gpos2;
	sfVertex vertex;
	int nodesW = gps_get_adjW();
	int nodesH = gps_get_adjH();
	vertex.color.r = 255;
	vertex.color.g = 128;
	vertex.color.b = 255;

	path_t road = gps_get_path();

	for(int i=0; i<(road.size)-1; i++)
	{
		grid_node_coord(plan, road.path[i], nodesW, nodesH, &(gpos2.x), &(gpos2.y));
		grid_node_coord(plan, road.path[i+1], nodesW, nodesH, &(gpos.x), &(gpos.y));

		vpos.x = (int)((gpos.x)/scale);
		vpos.y = H-1-(int)((gpos.y)/scale);				
		vertex.position = vpos;
		sfVertexArray_append(graphics_path, vertex);

		vpos.x = (int)((gpos2.x)/scale);
		vpos.y = W-1-(int)((gpos2.y)/scale);				
		vertex.position = vpos;
		sfVertexArray_append(graphics_path, vertex);
	}

	return true;
}

bool graphics_text_init()
{
	graphics_font = sfFont_createFromFile(GRAPHICS_FONT_ACCESS_PATH);
	graphics_coords_txt = sfText_create();
	graphics_gps_txt = sfText_create();

	if(!graphics_font || !graphics_coords_txt || !graphics_gps_txt) return false;

	sfText_setFont(graphics_coords_txt, graphics_font);
	sfText_setString(graphics_coords_txt, "");
	sfText_setPosition(graphics_coords_txt, (sfVector2f){10,10});
	sfText_setCharacterSize(graphics_coords_txt, 10);
	sfText_setColor(graphics_coords_txt, sfYellow);

	sfText_setFont(graphics_gps_txt, graphics_font);
	sfText_setString(graphics_gps_txt, "");
	sfText_setPosition(graphics_gps_txt, (sfVector2f){10,20});
	sfText_setCharacterSize(graphics_gps_txt, 10);
	sfText_setColor(graphics_gps_txt, sfYellow);

	return true;
}

bool graphics_objects_init()
{	
	if(!graphics_shapes_init()) return false;
	if(!graphics_grid_init()) return false;
	if(!graphics_path_init()) return false;

	graphics_haptic_left = sfRectangleShape_create();
	graphics_mouse_cursor = sfCircleShape_create();
	graphics_node_cursor = sfCircleShape_create();
	graphics_mouse_angle = sfVertexArray_create();
	graphics_gps_angle = sfVertexArray_create();

	if(!graphics_haptic_left || !graphics_mouse_cursor || !graphics_node_cursor || !graphics_mouse_angle || !graphics_gps_angle) return false;
				
	sfRectangleShape_setSize(graphics_haptic_left, (sfVector2f){W/4, 10});
	graphics_haptic_right = sfRectangleShape_copy(graphics_haptic_left);
	graphics_haptic_forw = sfRectangleShape_copy(graphics_haptic_left);

	sfRectangleShape_setPosition(graphics_haptic_left, (sfVector2f){0 + W/24, H-10});
	sfRectangleShape_setPosition(graphics_haptic_forw, (sfVector2f){W/3 + W/24, H-10});
	sfRectangleShape_setPosition(graphics_haptic_right, (sfVector2f){2*W/3 + W/24, H-10});
		
	sfCircleShape_setFillColor(graphics_mouse_cursor, sfCyan);
	sfCircleShape_setRadius(graphics_mouse_cursor, 4);

	sfCircleShape_setFillColor(graphics_node_cursor, sfRed);
	sfCircleShape_setRadius(graphics_node_cursor, 2);

	sfVertexArray_setPrimitiveType(graphics_mouse_angle, sfLines);

	sfVertexArray_setPrimitiveType(graphics_gps_angle, sfLines);

	graphics_update_location(0.1,0.1,0);
	graphics_update_gps_angle(0);

	return true;
}

void graphics_mouse_text(int x, int y)
{
	char txt_buf[256];

	map_t *plan = map_get_global();
	float scaleW = plan->width / W;
	float scaleH = plan->height / H;

	location_t new_loc;
	new_loc.pos.x = x * scaleH;
	new_loc.pos.y = (H - y) * scaleW;

	sprintf(txt_buf, "Mouse (%d,%d) | Map (%.3f,%.3f) | Node %d", x, H - y, new_loc.pos.x, new_loc.pos.y, grid_closest_node(plan, new_loc.pos));
		
	sfText_setString(graphics_coords_txt, txt_buf);
}

void graphics_update()
{
	sfRenderWindow_clear(graphics_window, sfBlack);

	sfRenderStates rs;
	rs.blendMode = sfBlendNone;
	rs.transform = sfTransform_Identity;
	rs.texture = NULL;
	rs.shader = NULL;

	sfRenderWindow_drawVertexArray(graphics_window, graphics_grid, &rs);
	sfRenderWindow_drawVertexArray(graphics_window, graphics_shapes, &rs);

	if(gps_is_running())
	{
		graphics_update_gps_angle((int)gps_get_angle());

		char txt_buf[256];
		sprintf(txt_buf, "Angle : %f | Node : %d | Node Step : %d | Dist Step %f\n", gps_get_angle(), gps_get_curr_node(), gps_get_curr_step(), gps_get_distance());		
		sfText_setString(graphics_gps_txt, txt_buf);

		graphics_path_update();
		sfRenderWindow_drawVertexArray(graphics_window, graphics_path, &rs);

		sfColor hapt_col = sfRed;
		hapt_col.a = haptic_get_left();
		sfRectangleShape_setFillColor(graphics_haptic_left, hapt_col);
		hapt_col.a = haptic_get_right();
		sfRectangleShape_setFillColor(graphics_haptic_right, hapt_col);
		hapt_col.a = haptic_get_forward();
		sfRectangleShape_setFillColor(graphics_haptic_forw, hapt_col);
		sfRenderWindow_drawRectangleShape(graphics_window, graphics_haptic_left, NULL);
		sfRenderWindow_drawRectangleShape(graphics_window, graphics_haptic_right, NULL);
		sfRenderWindow_drawRectangleShape(graphics_window, graphics_haptic_forw, NULL);
	}
	else
	{
		sfText_setString(graphics_gps_txt, "");
	}
	
	sfRenderWindow_drawCircleShape(graphics_window, graphics_mouse_cursor, &rs);
	sfRenderWindow_drawCircleShape(graphics_window, graphics_node_cursor, &rs);
	sfRenderWindow_drawVertexArray(graphics_window, graphics_mouse_angle, &rs);
	if(gps_is_running()) sfRenderWindow_drawVertexArray(graphics_window, graphics_gps_angle, &rs);

	sfRenderWindow_drawText(graphics_window, graphics_coords_txt, NULL);
	sfRenderWindow_drawText(graphics_window, graphics_gps_txt, NULL);

	sfRenderWindow_display(graphics_window);
}

bool graphics_real_init()
{
	/* Initialize render window */
	sfVideoMode vmode;
	vmode.width = W;
	vmode.height = H;
	vmode.bitsPerPixel = 8;

	sfContextSettings csettings;
	csettings.depthBits = 0;
	csettings.stencilBits = 0;
	csettings.antialiasingLevel = 0;
	csettings.majorVersion = 2;
	csettings.minorVersion = 0;
	
	if(!graphics_objects_init()) return false;
	if(!graphics_text_init()) return false;

	graphics_context = sfContext_create();
	graphics_window = sfRenderWindow_create(vmode, "montagne debug", sfTitlebar | sfClose, &csettings);

	if(!graphics_context || !graphics_window) return false;

	sfRenderWindow_setVerticalSyncEnabled(graphics_window, true);

	return true;
}

void graphics_run()
{
	printf("[GRAPHICS] Debug window started\n");

	/* this is required on unix systems */
	#ifdef __unix__ 
		XInitThreads();
	#endif

	if(!graphics_real_init())
	{
		printf("[GRAPHICS] Failed to load\n");
		return;
	}

	char txt_buf[256];

	sfEvent evt;
	while (sfRenderWindow_isOpen(graphics_window))
	{
		while (sfRenderWindow_pollEvent(graphics_window, &evt))
		{

			switch (evt.type)
			{
				/* Exiting window if requested */
				case sfEvtClosed:
					printf("[GRAPHICS] Exiting debug window\n");
					sfRenderWindow_close(graphics_window);
					break;

				/* Update location on mouse click */
				case sfEvtMouseButtonReleased:
					graphics_update_location(evt.mouseButton.x, evt.mouseButton.y, mouse_angle);
					break;

				case sfEvtMouseWheelMoved:
					mouse_angle += evt.mouseWheel.delta * 10;
					if(mouse_angle > 360) mouse_angle = mouse_angle % 360;
					if(mouse_angle < 0) mouse_angle = 360 + mouse_angle;
					graphics_update_mouse_angle(mouse_angle);
					break;
				
				case sfEvtMouseMoved:
					graphics_mouse_text(evt.mouseMove.x, evt.mouseMove.y);
					break;				

				default:
					break;
			}

		}

		graphics_update();		
	}

	/* nothing is cleaned, but I like to live dangerously */
	printf("[GRAPHICS] Exiting thread\n");
}

/* With SFML, we only can catch windows events from the same thread that created it. So we need to move every operation, from initialization to proper work into a separate thread */
bool graphics_init()
{
	graphics_thread =  sfThread_create(&graphics_run, NULL);
	if(!graphics_thread)
	{
		printf("[GRAPHICS] Failed to create the thread\n");
		return false;
	}
	
	sfThread_launch(graphics_thread);
}