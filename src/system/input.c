#include "input.h"

#include <SFML/System/Clock.h>
#include <SFML/System/Time.h>
#include <SFML/System/Sleep.h>

#include "configuration.h"

static sfKeyCode input_last_key;
static sfClock *input_timer_key;

bool input_init()
{
	input_timer_key = sfClock_create();
	input_last_key = sfKeyEscape;

	if(!input_timer_key) return false;

	return true;
}

sfKeyCode input_get_key()
{
	sfKeyCode key;
	bool got_key = false;

	sfClock_restart(input_timer_key);

loop:
	while(!got_key)
	{
		if(sfKeyboard_isKeyPressed(sfKeyUp))
		{
			key = sfKeyUp;
			break;
		}

		if(sfKeyboard_isKeyPressed(sfKeyDown))
		{
			key = sfKeyDown;
			break;
		}

		if(sfKeyboard_isKeyPressed(sfKeyRight))
		{
			key = sfKeyRight;
			break;
		}

		if(sfKeyboard_isKeyPressed(sfKeyLeft))
		{
			key = sfKeyLeft;
			break;
		}

		sfSleep(sfMilliseconds(CPU_WAIT_TIME_MSECS));
	}

	sfTime delta = sfClock_getElapsedTime(input_timer_key);
	if(sfTime_asMilliseconds(delta) >= INPUT_COOLDOWN_MSECS)
		return key;
	else goto loop;
}