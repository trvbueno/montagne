#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <stdbool.h>

/* Initializes and starts the graphics debug window */
bool graphics_init();

#endif