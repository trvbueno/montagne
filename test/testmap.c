#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include "../src/navigation/map/map.h"
#include "../src/navigation/map/grid.h"
#include "../src/navigation/pathfinding/astar.h"
#include "../src/navigation/pathfinding/salesman.h"
#include "../src/navigation/pathfinding/heuristic.h"
#include "../src/utils/matrix.h"
#include "limace.h"

/* Bresennham's version on Rosetta code, tailored for limace */
void line(Image im,char r,char g,char b, int x0, int y0, int x1, int y1)
{
	unsigned char **ir = ImGetR(im), **ig = ImGetG(im), **ib = ImGetB(im);
	int dx = abs(x1-x0), sx = x0<x1 ? 1 : -1;
	int dy = abs(y1-y0), sy = y0<y1 ? 1 : -1; 
	int err = (dx>dy ? dx : -dy)/2, e2;
 
	for(;;){
		if ((x0 >= 0) && (x0 < ImNbCol(im)) && (y0 >= 0) && (y0 < ImNbRow(im)))
		{
    		ir[y0][x0] = r;
    		ig[y0][x0] = g;
    		ib[y0][x0] = b;
    	}
    	if (x0==x1 && y0==y1) break;
    	e2 = err;
    	if (e2 >-dx) { err -= dy; x0 += sx; }
    	if (e2 < dy) { err += dx; y0 += sy; }
  	}
}

// TODO: when some maps are at 1 level of subdiv, the software crashes

int main (void) {
	// import the map used for testing purposes
	map_t *plan = map_import_room_yaml("../res/supertest.yaml");
	matrix adjacence = NULL;
	int W, H;
	path_t p/*, opti*/;
	clock_t timer_start, timer_end;

	grid_gridify(plan, &adjacence, &W, &H);

	if (! plan) return 2;

	// check some informations
	puts("------------------------------------------ M A P");
	printf("MAP: %s (%fm x %fm)\n", plan->name, plan->width, plan->height);
	printf("  + %d subdivision(s)\n", plan->m_unit);
	printf("  + %d object(s)\n", plan->obj_count);

	for (int o = 0; o < plan->obj_count; ++o) {
		printf("  + OBJECT: %s (%d points)\n", plan->obj[o].name, plan->obj[o].nb_points);
		printf("     %s\n", plan->obj[o].description);
		for (int n = 0; n < plan->obj[o].nb_points; ++n)
			printf("     %d: [%4f, %4f]\n", n, plan->obj[o].mesh[n].x, plan->obj[o].mesh[n].y);
	}
	printf("  + SENSOR 1: [%4f, %4f]\n", plan->sensor1.x, plan->sensor1.y);
	printf("  + SENSOR 2: [%4f, %4f]\n", plan->sensor2.x, plan->sensor2.y);
	printf("  + SENSOR 3: [%4f, %4f]\n", plan->sensor3.x, plan->sensor3.y);

	puts("---------------------------------------- G R I D");
	printf("W = %d, H = %d\n", W, H);
	// matx_print(adjacence, W*H, W*H);

	// and trace it on an output image of width 1000px and same ratio
	const int imwidth = 1400;
	const int imheight = imwidth*(plan->height/plan->width);
	Image i_output = ImAlloc(Color, imheight, imwidth);

	unsigned char **r = ImGetR(i_output), **g = ImGetG(i_output), **b = ImGetB(i_output);
	float scale = plan->width / imwidth;
	point_t position, p2;

	for (int i = 0; i < imwidth; ++i) {
		for (int j = 0; j < imheight; ++j) {
			// adapt to resolution
			position.x = i*scale;
			position.y = plan->height - j*scale;
			// draw
			if (map_accessible(plan, &position)) {
				r[j][i] = 0;
				g[j][i] = 0;
				b[j][i] = 0;
			}
			else {
				r[j][i] = 64;
				g[j][i] = 64;
				b[j][i] = 64;
			}
		}
	}

	puts("hop !");
	// print the nodes
	for (int no = 0; no < W*H; ++no) {
		grid_node_coord(plan, no, W, H, &(position.x), &(position.y));
		for (int l = 0; l < W*H; ++l) {
			if (adjacence[matx_index(no, l, W*H, W*H)] > 0.001) {
				grid_node_coord(plan, l, W, H, &(p2.x), &(p2.y));
				line(i_output, 32,32,32,
					(int)((position.x)/scale),
					imheight-1-(int)((position.y)/scale),
					(int)((p2.x)/scale),
					imheight-1-(int)((p2.y)/scale)
					);
			}
		}
		r[imheight-1-(int)((position.y)/scale)][(int)((position.x)/scale)] = 174;
		g[imheight-1-(int)((position.y)/scale)][(int)((position.x)/scale)] = 174;
		b[imheight-1-(int)((position.y)/scale)][(int)((position.x)/scale)] = 174;
	}

	// print the dots
	for (int o = 0; o < plan->obj_count; ++o) {
		for (int p = 0; p < plan->obj[o].nb_points; ++p) {
			r[imheight-1-(int)((plan->obj[o].mesh[p].y)/scale)][(int)((plan->obj[o].mesh[p].x)/scale)] = 0;
			g[imheight-1-(int)((plan->obj[o].mesh[p].y)/scale)][(int)((plan->obj[o].mesh[p].x)/scale)] = 255;
			b[imheight-1-(int)((plan->obj[o].mesh[p].y)/scale)][(int)((plan->obj[o].mesh[p].x)/scale)] = 0;
			if (p == 0)
				line(i_output, 128, 255, 128,
					(int)((plan->obj[o].mesh[0].x)/scale),
					imheight-1-(int)((plan->obj[o].mesh[0].y)/scale),
					(int)((plan->obj[o].mesh[plan->obj[o].nb_points-1].x)/scale),
					imheight-1-(int)((plan->obj[o].mesh[plan->obj[o].nb_points-1].y)/scale)
					);
			else
				line(i_output, 128, 255, 128,
					(int)((plan->obj[o].mesh[p-1].x)/scale),
					imheight-1-(int)((plan->obj[o].mesh[p-1].y)/scale),
					(int)((plan->obj[o].mesh[p].x)/scale),
					imheight-1-(int)((plan->obj[o].mesh[p].y)/scale)
					);
		}
	}

	int i;

	/*
	matrix proxy_object = salesman_proxy_graph(plan);
	opti = salesman_greedy_path(proxy_object, plan->obj_count, 2, &len);
	for (i=0; i<opti.size; i++) printf("%d",opti.path[i]); printf("\n");

	// SALESMAN
	i = 0;
	while (i < opti.size-1) {
		// printf("%d ", opti.path[0]);
		position = plan->obj[opti.path[i]].door;
		p2 = plan->obj[opti.path[i+1]].door;
		printf("(%f, %f) to", p2.x, p2.y);
		printf(" (%f %f)\n", position.x, position.y);
		line(i_output, 255,255,100,
			(int)((position.x)/scale),
			imheight-1-(int)((position.y)/scale),
			(int)((p2.x)/scale),
			imheight-1-(int)((p2.y)/scale)
			);
		++ i;
	} */


	// BIG ASTAR TEST
	printf ("ASTAR\n");
	timer_start = clock();

	int
	A = grid_closest_node_failsafe(plan, adjacence, W, H, plan->obj[5].door),
	B = grid_closest_node_failsafe(plan, adjacence, W, H, plan->obj[6].door),
	C = grid_closest_node_failsafe(plan, adjacence, W, H, plan->obj[2].door);

	p = astar_path(adjacence, W, H, A, B);

	// printf("A = %d, B = %d, C = %d\n", A, B, C);

	timer_end = clock();

	// the first value of path is its size
	i = 1;
	while (i < p.size) {
		// printf("%d ", p.path[i]);
		grid_node_coord(plan, p.path[i], W, H, &(position.x), &(position.y));
		grid_node_coord(plan, p.path[i-1], W, H, &(p2.x), &(p2.y));
		line(i_output, 100,255,255,
			(int)((position.x)/scale),
			imheight-1-(int)((position.y)/scale),
			(int)((p2.x)/scale),
			imheight-1-(int)((p2.y)/scale)
			);
		++ i;
	}
	printf("\n");

	printf("Elapsed time for A*: %f\n", ((double) (timer_end - timer_start))/CLOCKS_PER_SEC);
	printf("Elapsed total time:  %f\n", ((double) timer_end)/CLOCKS_PER_SEC);

	free(p.path);
	/*
	p = astar_path(adjacence, W, H, B, C);
	i = 1;
	while (i < p.size) {
		// printf("%d ", p.path[i]);
		grid_node_coord(plan, p.path[i], W, H, &(position.x), &(position.y));
		grid_node_coord(plan, p.path[i-1], W, H, &(p2.x), &(p2.y));
		line(i_output, 255,100,255,
			(int)((position.x)/scale),
			imheight-1-(int)((position.y)/scale),
			(int)((p2.x)/scale),
			imheight-1-(int)((p2.y)/scale)
			);
		++ i;
	}
	// printf("\n");
	free(p.path);*/

	matx_free(adjacence);
	// free(opti.path);
	// matx_free(proxy_object);
	ImWrite(i_output, "mapgrid.pgm");
	ImFree(&i_output);
	map_free(plan);
	return 0;
}