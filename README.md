# montagne

Montagne is an embedded blind guide, suitable for leading visually impaired people in exhibitions, based on Raspberry PI.

This repository contains the software of the main application, the configuration of the Raspberry Pi system and the hardware schematics.

This project is part of the license 2 "Projet Logiciel" course at Université Toulouse III, France.

Quick overview :

- Smart belt system
- Raspberry PI
- Vibration & audio based UI
- SFML

> On va s'aimer ! - Gilbert Montagné

### Version
Work in progress

### License
LGPL 3.0
